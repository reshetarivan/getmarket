## Stage 1 - the build process
#FROM node:12.13.1 as build-deps
#WORKDIR /usr/src/app
#COPY package.json package-lock.json ./
#RUN npm i
#COPY . ./
#RUN npm run build --prod

## Stage 2 - the production environment
# Start from latest nginx
#FROM nginx:1.12-alpine
#COPY nginx.conf /etc/nginx/nginx.conf
#COPY --from=build-deps /usr/src/app/build /usr/share/nginx/html
#EXPOSE 80 443

# Copy .env file and shell script to container
#WORKDIR /usr/share/nginx/html
#COPY ./env.sh .
#COPY .env .

# Add bash
#RUN apk add --no-cache bash

# Make our shell script executable
#RUN chmod +x env.sh

# Start up nginx server
#CMD ["/bin/bash", "-c", "/usr/share/nginx/html/env.sh && nginx -g \"daemon off;\""]
#CMD ["/bin/bash", "-c", "env.sh"]
#ARG STAGE="development"
#ENV STAGE=$STAGE
