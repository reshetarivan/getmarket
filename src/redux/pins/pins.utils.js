export const selectPinUtil = (pins, pin) => {
  const pinExistsInArray = pins.find(
    pinItem => pinItem.shopId === pin.shopId
  );
  if (!pinExistsInArray) {
    return [...pins, pin]
  }
  return pins.filter(pinItem =>
    pinItem.shopId !== pin.shopId
  );
};

export const updatePinUtil = (pins, pin) => {
  if (pins) {
    const newPins = pins.map(shop => {
      if (shop.shopId === pin.id) {
        shop.shopRating = pin.rating;
        shop.shopReviews = pin.reviews;
      }
      return shop;
    });
    console.log({newPins});
    return newPins;
  }
};

export const filterShops = (shops, priceRange) => {

  shops.totalItems = 0;
  shops.data = shops.data.filter((shop, shopIdx) => {
    shop.items = shop.items.filter((item) =>
      (parseFloat(item.pinPrice) >= parseFloat(priceRange.fromPrice) &&
        parseFloat(item.pinPrice) <= parseFloat(priceRange.toPrice))
    );
    if (!shop.items.length) {
      delete shops.data[shopIdx];
      return false;
    } else {
      shops.totalItems += shop.items.length;
      return shop;
    }
  });
  shops.totalShops = shops.data.length;

  return shops;
};
