import {takeLatest, put, all, call, select} from 'redux-saga/effects';
import {PinsTypes} from "./pins.types";
import axios from "axios";
import _ from 'lodash';
import {MainConfig} from "../../config/main.config";
import {failGetPinsAction, successGetPinsAction} from "./pins.actions";
import {stringify} from "query-string";
import {selectFiltersAll} from "../filters/filters.selectors";
import {setPriceRangeAction} from "../filters/filters.actions";
import {filterShops} from "./pins.utils";


export function* fetchPinsAsync() {
  try {
    const payload = yield select(selectFiltersAll);
    let newPayload = {};
    for (const elem in payload) {
      if (elem !== 'isError' &&
        elem !== 'isLoading' &&
        elem !== 'searchImg' &&
        elem !== 'suggestions' &&
        elem !== 'options' &&
        elem !== 'minPrice' &&
        elem !== 'maxPrice' &&
        elem !== 'fromPrice' &&
        elem !== 'toPrice'
      ){
        newPayload[elem] = payload[elem];
      }
    }
    const query = stringify(newPayload);
    const shops =  yield axios.get(`${MainConfig.DOMAIN_URL}/pins/?${query}`)
      .then(res => res.data);

    let filteredShops = {};
    if (shops.data.length) {
      let minPrice = 0;
      let maxPrice = 0;
      let fromPrice = 0;
      let toPrice = 0;
      yield shops.data.map(shopsData => {

        const minPriceObj = _.minBy(shopsData.items, e => parseFloat(e.pinPrice));
        const maxPriceObj = _.maxBy(shopsData.items, e => parseFloat(e.pinPrice));
        if (parseFloat(minPriceObj.pinPrice) < maxPrice) {
          minPrice = parseFloat(minPriceObj.pinPrice);
        }
        if (parseFloat(maxPriceObj.pinPrice) > maxPrice) {
          maxPrice = parseFloat(maxPriceObj.pinPrice);
        }
        fromPrice = parseFloat(payload.fromPrice);
        toPrice = parseFloat(payload.toPrice);

        if (fromPrice <= minPrice || fromPrice >= maxPrice || !fromPrice) {
          fromPrice = minPrice;
        }
        if (toPrice >= maxPrice || !toPrice) {
          toPrice = maxPrice;
        }

      });

      const priceRange = { minPrice, maxPrice, fromPrice, toPrice };
      yield put(setPriceRangeAction(priceRange));
      filteredShops = yield filterShops(shops, priceRange);
    }
    yield put(successGetPinsAction(filteredShops));

  } catch (error) {
    yield put(failGetPinsAction(error.message))
  }
}

export function* onStartGetPins() {
  yield takeLatest(
    PinsTypes.START_GET_PINS,
    fetchPinsAsync
  )
}

export function* pinsSagas() {
  yield all([
    call(onStartGetPins)
  ])
}
