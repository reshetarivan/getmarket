import {FiltersTypes} from "./filters.types";

export const selectSuggestionAction = suggestion => ({
  type: FiltersTypes.SELECT_SUGGESTION,
  payload: suggestion
});

export const startGetSuggestionAction = () => ({
  type: FiltersTypes.START_GET_SUGGESTION,
  payload: []
});

export const successGetSuggestionAction = (suggestionData) => ({
  type: FiltersTypes.SUCCESS_GET_SUGGESTIONS,
  payload: suggestionData
});

export const failGetSuggestionAction = error => ({
  type: FiltersTypes.FAIL_GET_SUGGESTION,
  payload: error
});

export const inputSearchAction = searchRequest => ({
  type: FiltersTypes.INPUT_SEARCH,
  payload: searchRequest
});

export const clearSearchAction = () => ({
  type: FiltersTypes.CLEAR_SEARCH,
});

export const setTypeAction = type => ({
  type: FiltersTypes.SET_TYPE,
  payload: type
});

export const setCategoryAction = category => ({
  type: FiltersTypes.SET_CATEGORY,
  payload: category
});

export const fetchCategoriesAction = search => ({
  type: FiltersTypes.START_FETCH_CATEGORIES,
  payload: search
});

export const successFetchCategoriesAction = categories => ({
  type: FiltersTypes.SUCCESS_FETCH_CATEGORIES,
  payload: categories
});

export const failFetchCategoriesAction = error => ({
  type: FiltersTypes.FAIL_FETCH_CATEGORIES,
  payload: error
});

export const setPriceRangeAction = priceRange => ({
  type: FiltersTypes.SET_PRICE_RANGE,
  payload: priceRange
});

export const updatePriceRangeAction = priceRange => ({
  type: FiltersTypes.UPDATE_PRICE_RANGE,
  payload: priceRange
});

export const clearFiltersAction  = () => ({
  type: FiltersTypes.CLEAR_FILTERS,
});
