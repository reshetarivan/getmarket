import {createSelector} from "reselect";

const selectFilters = state => state.filters;

export const selectFiltersAll = createSelector(
  [selectFilters],
  filters => filters
);

export const selectFiltersSearch = createSelector(
  [selectFilters],
  filters => filters.search
);

export const selectFiltersType = createSelector(
  [selectFilters],
  filters => filters.type
);

export const selectFiltersCategory = createSelector(
  [selectFilters],
  filters => filters.category
);

export const selectFiltersSearchImg = createSelector(
  [selectFilters],
  filters => filters.searchImg
);

export const selectFiltersSuggestions = createSelector(
  [selectFilters],
  filters => filters.suggestions
);

export const selectFiltersIsLoading = createSelector(
  [selectFilters],
  filters => filters.isLoading
);

export const selectFiltersIsError = createSelector(
  [selectFilters],
  filters => filters.isError
);

export const selectFiltersCategories = createSelector(
  [selectFilters],
  filters => filters.options.categories.data
);

export const selectPriceRange = createSelector(
  [selectFilters],
  filters => filters.minPrice
);

