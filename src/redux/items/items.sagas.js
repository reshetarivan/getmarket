import {takeLatest, put, all, call, select} from 'redux-saga/effects';
import {ItemsTypes} from "./items.types";
import axios from "axios";
import _ from 'lodash';
import {MainConfig} from "../../config/main.config";
import {failGetItemsAction, successGetItemsAction} from "./items.actions";
import {stringify} from "query-string";
import {selectFiltersAll} from "../filters/filters.selectors";
import {setPriceRangeAction} from "../filters/filters.actions";
import {filterItems} from "./items.utils";


export function* fetchItemsAsync() {
  try {
    const payload = yield select(selectFiltersAll);
    let newPayload = {};
    for (const elem in payload) {
      if (elem !== 'isError' &&
        elem !== 'isLoading' &&
        elem !== 'searchImg' &&
        elem !== 'suggestions' &&
        elem !== 'options' &&
        elem !== 'minPrice' &&
        elem !== 'maxPrice' &&
        elem !== 'fromPrice' &&
        elem !== 'toPrice'
      ){
        newPayload[elem] = payload[elem];
      }
    }
    const query = stringify(newPayload);
    const shops =  yield axios.get(`${MainConfig.DOMAIN_URL}/items/?${query}`)
      .then(res => res.data);

    let filteredShops = {};
    if (shops.data.length) {
      let minPrice = 0;
      let maxPrice = 0;
      let fromPrice = 0;
      let toPrice = 0;
      // yield shops.data.map(shopsData => {
      //
      //
      //
      // });

      const minPriceObj = _.minBy(shops.data, e => parseFloat(e.price));
      const maxPriceObj = _.maxBy(shops.data, e => parseFloat(e.price));
      if (parseFloat(minPriceObj.price) < maxPrice) {
        minPrice = parseFloat(minPriceObj.price);
      }
      if (parseFloat(maxPriceObj.price) > maxPrice) {
        maxPrice = parseFloat(maxPriceObj.price);
      }
      fromPrice = parseFloat(payload.fromPrice);
      toPrice = parseFloat(payload.toPrice);

      if (fromPrice <= minPrice || fromPrice >= maxPrice || !fromPrice) {
        fromPrice = minPrice;
      }
      if (toPrice >= maxPrice || !toPrice) {
        toPrice = maxPrice;
      }

      const priceRange = { minPrice, maxPrice, fromPrice, toPrice };
      yield put(setPriceRangeAction(priceRange));
      filteredShops = yield filterItems(shops, priceRange);
    }
    yield put(successGetItemsAction(filteredShops));

  } catch (error) {
    yield put(failGetItemsAction(error.message))
  }
}

export function* onStartGetItems() {
  yield takeLatest(
    ItemsTypes.START_GET_ITEMS,
    fetchItemsAsync
  )
}

export function* itemsSagas() {
  yield all([
    call(onStartGetItems)
  ])
}
