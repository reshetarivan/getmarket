import {ItemsTypes} from "./items.types";

export const startGetItemsAction = (query) => ({
  type: ItemsTypes.START_GET_ITEMS,
  payload: query
});

export const successGetItemsAction = items => ({
  type: ItemsTypes.SUCCESS_GET_ITEMS,
  payload: items
});

export const failGetItemsAction = error => ({
  type: ItemsTypes.FAIL_GET_ITEMS,
  payload: error
});

export const setIsHoverAction = (shop) => ({
  type: ItemsTypes.SET_IS_HOVER,
  payload: shop
});

export const updateItemAction = (shop) => ({
  type: ItemsTypes.UPDATE_ITEM,
  payload: shop
});
