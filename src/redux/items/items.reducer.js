import {ItemsTypes} from "./items.types";
import {updateItemUtil} from "./items.utils";

const INITIAL_STATE = {
  items: {
    start: 0,
    total: 0,
    data: []
  },
  isLoading: false,
  isError: false,
  isHover: null,
};

const itemsReducer = (state = INITIAL_STATE, action) => {

  switch (action.type) {

    case ItemsTypes.START_GET_ITEMS:
      return {
        ...state,
        isError: false,
        isLoading: true,
        items: {
          start: 0,
          total: 0,
          data: []
        }
      };

    case ItemsTypes.SUCCESS_GET_ITEMS:
      return {
        ...state,
        isError: false,
        isLoading: false,
        items: action.payload
      };

    case ItemsTypes.FAIL_GET_ITEMS:
      return {
        ...state,
        isError: action.payload,
        isLoading: false,
        items: {
          start: 0,
          total: 0,
          data: []
        }
      };

    case ItemsTypes.SET_IS_HOVER:
      return {
        ...state,
        isHover: action.payload,
      };

    case ItemsTypes.UPDATE_ITEM:
      console.log('UPDATE_ITEM', action);
      return {
        ...state,
        items: {
          ...state.items,
          data: updateItemUtil(state.items.data, action.payload)
        }
      };

    default:
      return state;
  }
};

export default itemsReducer;
