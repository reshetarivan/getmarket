import {createSelector} from "reselect";

const selectItems = state => state.items;

// export const selectItemsAll = createSelector(
//   [selectItems],
//   items => items
// );

// export const selectItem = pinIdx => createSelector(
//   [selectItems],
//   items => pins.pins.data ? pins.pins.data[pinIdx] : null
// );

export const selectPinsIsLoading = createSelector(
  [selectPins],
  pins => pins.isLoading
);

export const selectPinsIsError = createSelector(
  [selectPins],
  pins => pins.isError
);

export const selectSelectedPins = createSelector(
  [selectPins],
  pins => pins.selected
);

export const selectIsSelectAll = createSelector(
  [selectPins],
  pins => pins.selectAll
);

export const selectPinsIsHover = createSelector(
  [selectPins],
  pins => pins.isHover
);

