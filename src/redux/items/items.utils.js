// export const selectItemUtil = (pins, pin) => {
//   const pinExistsInArray = pins.find(
//     pinItem => pinItem.shopId === pin.shopId
//   );
//   if (!pinExistsInArray) {
//     return [...pins, pin]
//   }
//   return pins.filter(pinItem =>
//     pinItem.shopId !== pin.shopId
//   );
// };

export const updateItemUtil = (pins, pin) => {
  if (pins) {
    const newPins = pins.map(shop => {
      if (shop.shopId === pin.id) {
        shop.shopRating = pin.rating;
        shop.shopReviews = pin.reviews;
      }
      return shop;
    });
    console.log({newPins});
    return newPins;
  }
};

export const filterItems = (items, priceRange) => {
  items.total = 0;
  items.data = items.data.filter((item, itemIdx) => {
     if (parseFloat(item.price) >= parseFloat(priceRange.fromPrice) &&
        parseFloat(item.price) <= parseFloat(priceRange.toPrice)) {
       if (!items.data.length) {
         delete items.data[itemIdx];
         return false;
       } else {
         items.total += items.data.length;
         return item;
       }
     }
  });
  items.total = items.data.length;
  return items;
};
