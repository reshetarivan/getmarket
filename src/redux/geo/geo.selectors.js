import {createSelector} from "reselect";

const selectGeo = state => state.geo;

export const selectGeoLatLng = createSelector(
  [selectGeo],
  geo => ({lat: geo.lat, lng: geo.lng})
);
