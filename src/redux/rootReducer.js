import { combineReducers } from 'redux';
import { persistReducer } from "redux-persist";
import storage from 'redux-persist/lib/storage';
import filtersReducer from "./filters/filters.reducer";
import pinsReducer from "./pins/pins.reducer";
import itemsReducer from "./items/items.reducer";
import geoReducer from "./geo/geo.reducer";
import reviewsReducer from "./reviews/reviews.reducer";
import {reducer as toastrReducer} from 'react-redux-toastr';


const persistConfig = {
  key: 'root',
  storage,
  whitelist: ['filters', 'pins', 'geo', 'reviews', 'items']
};

const rootReducer = combineReducers({
  filters: filtersReducer,
  pins: pinsReducer,
  items: itemsReducer,
  geo: geoReducer,
  reviews: reviewsReducer,
  toastr: toastrReducer
});

export default persistReducer(persistConfig, rootReducer);
