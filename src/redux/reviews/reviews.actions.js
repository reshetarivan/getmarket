import {ReviewsTypes} from "./reviews.types";

export const startGetReviewsAction = () => ({
  type: ReviewsTypes.START_GET_REVIEWS,
});

export const successGetReviewsAction = reviews => ({
  type: ReviewsTypes.SUCCESS_GET_REVIEWS,
  payload: reviews
});

export const failGetReviewsAction = error => ({
  type: ReviewsTypes.FAIL_GET_REVIEWS,
  payload: error
});

export const setIsOpenAction = (isOpen, shop) => ({
  type: ReviewsTypes.SET_IS_OPEN,
  payload: {isOpen, shop}
});

export const addReviewAction = (formData) => ({
  type: ReviewsTypes.ADD_REVIEW,
  payload: formData
});

export const updateReviewAction = (formData) => ({
  type: ReviewsTypes.UPDATE_REVIEW,
  payload: formData
});

export const successAddReviewAction = (message) => ({
  type: ReviewsTypes.SUCCESS_ADD_REVIEW,
  payload: message
});

export const failAddReviewsAction = error => ({
  type: ReviewsTypes.FAIL_ADD_REVIEW,
  payload: error
});

