import {ReviewsTypes} from "./reviews.types";

const INITIAL_STATE = {
  start: 0,
  total: 0,
  data: [],
  isLoading: false,
  isError: false,
  isSuccess: false,
  isOpen: false,
  shopReview: null,
  rating: 0,
  rates: [
    {rate: 1, val: 0},
    {rate: 2, val: 0},
    {rate: 3, val: 0},
    {rate: 4, val: 0},
    {rate: 5, val: 0},
  ]
};

const reviewsReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {

    case ReviewsTypes.START_GET_REVIEWS:
      return {
        ...state,
        start: INITIAL_STATE.start,
        total: INITIAL_STATE.total,
        data: INITIAL_STATE.data,
        rating: INITIAL_STATE.rating,
        rates: INITIAL_STATE.rates,
        isLoading: true,
        isError: false,
      };

    case ReviewsTypes.SUCCESS_GET_REVIEWS:
      return {
        ...state,
        total: action.payload.total,
        data: action.payload.data,
        rating: action.payload.rating,
        rates: action.payload.rates,
        isLoading: false,
        isError: false,
      };

    case ReviewsTypes.FAIL_GET_REVIEWS:
      return {
        ...state,
        total: INITIAL_STATE.total,
        data: INITIAL_STATE.data,
        rating: INITIAL_STATE.rating,
        rates: INITIAL_STATE.rates,
        isLoading: false,
        isError: action.payload,
      };

    case ReviewsTypes.SET_IS_OPEN:
      return {
        ...state,
        isOpen: action.payload.isOpen,
        shopReview: action.payload.shop
      };

    case ReviewsTypes.ADD_REVIEW:
      return {
        ...state,
        isLoading: true,
        isSuccess: false,
        isError: false,
      };

    case ReviewsTypes.UPDATE_REVIEW:
      return {
        ...state,
        isLoading: true,
        isSuccess: false,
        isError: false,
      };

    case ReviewsTypes.SUCCESS_ADD_REVIEW:
      return {
        ...state,
        isLoading: false,
        isSuccess: action.payload,
        isError: false,
      };

    case ReviewsTypes.FAIL_ADD_REVIEW:
      return {
        ...state,
        isLoading: false,
        isSuccess: false,
        isError: action.payload,
      };

    default:
      return state;
  }
};

export default reviewsReducer;
