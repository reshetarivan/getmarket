import {createSelector} from "reselect";

const selectReviews = state => state.reviews;

export const selectReviewsAll = createSelector(
  [selectReviews],
  reviews => reviews
);

export const selectIsOpen = createSelector(
  [selectReviews],
  reviews => reviews.isOpen
);

export const selectShopReview = createSelector(
  [selectReviews],
  reviews => reviews.shopReview
);
