let configVars = {
  DOMAIN_URL: 'http://localhost:4000'
};

if(window?._env_?.API_URL) {
  configVars.DOMAIN_URL = window._env_.API_URL
}

export const MainConfig = {

  ...configVars,
  REQUEST_DELAY: 500, //milliseconds

  // Media Breakpoints
  XS: '(max-width:576px)',
  SM: '(min-width:576px)',
  MD: '(min-width:768px)',
  LG: '(min-width:992px)',
  XL: '(min-width:1200px)',

};
