export default (state, action) => {
  switch (action.type) {
    case 'countries':
      return {
        ...state,
        countries: action.newOptions
      };
    case 'regions':
      return {
        ...state,
        regions: action.newOptions
      };
    case 'cities':
      return {
        ...state,
        cities: action.newOptions
      };
    case 'categories':
      return {
        ...state,
        categories: action.newOptions
      };
    default:
      return state;
  }
};
