export default (state, action) => {
  switch (action.type) {
    case 'latitude':
      return {
        ...state,
        lat: action.newFilter
      };
    case 'longitude':
      return {
        ...state,
        lng: action.newFilter
      };
    case 'geo':
      return action.newFilter;
    default:
      return state;
  }
};
