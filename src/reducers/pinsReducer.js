export default (state, action) => {
  let idxSel = state.indexOf(action.selPin);
  let idx = state.indexOf(action.newPin);
  let outidx = state.indexOf(action.outPin);

  switch (action.type) {
    case 'selectPin':
      if (!state[idxSel].selected) {
        state[idxSel] = {...state[idxSel], selected: true};
      } else {
        state[idxSel] = {...state[idxSel], selected: false};
      }
      return state;
    case 'hoverPin':
      state[idx] = {...state[idx], hover: true};
      return state;
    case 'outPin':
      delete state[outidx].hover;
      return state;
    case 'pins':
      return action.newPins;
    default:
      return state;
  }
};
