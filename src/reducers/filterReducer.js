export default (state, action) => {
  switch (action.type) {

    case 'country':
      return { ...state, country: action.newFilter };

    case 'region':
      return { ...state, region: action.newFilter };

    case 'city':
      return { ...state, city: action.newFilter };

    case 'type':
      return { ...state, type: action.newFilter };

    case 'search':
      return { ...state, search: action.newFilter };

    case 'searchImg':
      return { ...state, searchImg: action.newFilter };

    case 'category':
      return { ...state, category: action.newFilter };

    case 'minPrice':
      return { ...state, minPrice: action.newFilter };

    case 'maxPrice':
      return { ...state, maxPrice: action.newFilter };

    case 'fromPrice':
      return { ...state,  fromPrice: action.newFilter };

    case 'toPrice':
      return { ...state, toPrice: action.newFilter };

    case 'filters':
      return action.newFilter;

    default:
      return state;
  }
};
