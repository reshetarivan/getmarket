import themeReducer from './themeReducer';
import filterOptionsReducer from './filterOptionsReducer';
import filterReducer from './filterReducer';
import errorsReducer from './errorsReducer';
import pinsReducer from './pinsReducer';
import geoReducer from "./geoReducer";

const mainReducer = (state, action) => {
  return {
    ...state,
    theme: themeReducer(state.theme, action),
    filterOptions: filterOptionsReducer(state.filterOptions, action),
    filters: filterReducer(state.filters, action),
    errors: errorsReducer(state.errors, action),
    pins: pinsReducer(state.pins, action),
    geo: geoReducer(state.geo, action)
  }
};

export default mainReducer;
