export default (state, action) => {
  switch (action.type) {
    case 'error_city':
      return {
        ...state,
        city: action.newError
      };
    case 'error_search':
      return {
        ...state,
        search: action.newError
      };
    case 'errors':
      return {
        ...state,
        all: action.newError
      };
    case 'clear':
      return {};
    default:
      return {};
  }
};
