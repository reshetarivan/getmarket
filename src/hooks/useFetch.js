import {useEffect, useState} from 'react';
import axios from 'axios';

const useFetch = (source) => {

  const [datas, setData] = useState([]);
  const fetchResource = async (source) => {

    if (source) {
      await axios.get(`http://localhost:4000/${source}`)
        .then((response) => {
          if (response.data !== 'Not found') {
            setData(response.data);
          } else {
            setData([]);
          }
        })
        .catch(() => {
          setData([]);
        });
    }
  };

  useEffect(() => {
    fetchResource(source);
  }, [source]);

  return datas;

};

export default useFetch;
