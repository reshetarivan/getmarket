import {useState, useEffect} from 'react';
import axios from 'axios';
import {useStateValue} from "../state";

const useRemoteFetch = (source, param='') => {

  const [data, setData] = useState([]);
  const [dispatch] = useStateValue();


  const fetchResource = async (source) => {
    if (source) {
      await axios.get(source)
        .then((response) => {

          if (param === 'cityLocation') {
            console.log('response useRemoteFetch', response);
            console.log('LAT', response.data.results[0].geometry.location.lat);
            console.log('LNG', response.data.results[0].geometry.location.lng);

            dispatch({
              type: 'latitude',
              newFilter: response.data.results[0].geometry.location.lat
            });
            dispatch({
              type: 'longitude',
              newFilter: response.data.results[0].geometry.location.lng
            });
          }
          setData(response);
        })
        .catch(() => {
          setData([]);
        });
    }
  };

  useEffect(() => {
    fetchResource(source);
  }, [source]);

  return data;

};

export default useRemoteFetch;
