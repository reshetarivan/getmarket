import {useEffect, useState} from 'react';

const useSelectPin = (pin) => {

  const [selectedPins, setSelectedPins] = useState({});
  useEffect(() => {
    setSelectedPins(oldPins => ({
      ...oldPins,
      pin
    }))
  },[pin]);
  console.log('selectedPins', selectedPins);


  return selectedPins;
};

export default useSelectPin;
