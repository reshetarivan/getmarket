import {useState, useEffect} from 'react';
import axios from 'axios';

const useFilters = (resource) => {
  const [filters, setFilter] = useState([]);

  const fetchResource = async (resource) => {
    const response = await axios.get(`https://jsonplaceholder.typicode.com/${resource}`);
    setFilter(response.data);
  };

  useEffect(() => {
    fetchResource(resource);
  }, [resource]);

  return filters;

};

export default useFilters;
