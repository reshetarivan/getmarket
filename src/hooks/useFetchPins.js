import {useEffect} from 'react';
import axios from 'axios';
import _ from 'lodash';
import {useStateValue} from "../state";
import {stringify} from "query-string";

const useFetch = (setPinsLoading) => {

  const [mainState, dispatch] = useStateValue();

  const removeDuplicates = (arr) => {
    return arr.filter((item, index) => {
      return index === arr.findIndex(obj => {
        return JSON.stringify(obj.id) === JSON.stringify(item.id);
      });
    });
  };

  const fetchResource = () => {

    axios.get(`http://localhost:4000/pins/?${stringify(mainState.filters)}&${stringify(mainState.geo)}`)
        .then((response) => {
          if (response.data !== 'Not found') {
            const pins_array = _.union(mainState.pins, response.data);
            let unique_array = removeDuplicates(pins_array);

            let sortedItems = [];
            const getLength = (item) => {
              let len = Math.sqrt(Math.pow(parseFloat(item.lat) - mainState.geo.lat, 2) + Math.pow(parseFloat(item.lng) - mainState.geo.lng, 2));
              sortedItems.push({...item, len});
            };
            unique_array.forEach(getLength);
            sortedItems.sort(function compare(a, b){return a.len - b.len;});
            let filtered = sortedItems;
            if (mainState.filters.fromPrice && mainState.filters.toPrice) {
              filtered = sortedItems.filter(item => {
                return parseFloat(item.price) >= mainState.filters.fromPrice &&
                  parseFloat(item.price) <= mainState.filters.toPrice ||
                  item.price === null;
              });
            }

            console.log('fromPrice', mainState.filters.fromPrice);
            console.log('toPrice', mainState.filters.toPrice);
            console.log('sortedItems', filtered);

            dispatch({
              type: 'pins',
              newPins: filtered
            });

            window.history.replaceState(mainState.filters, "MarketMap", `?${stringify(mainState.filters)}&${stringify(mainState.geo)}`);

          } else {
            dispatch({
              type: 'pins',
              newPins: []
            });
          }
        })
        .catch(e => {
          console.error(e);
          dispatch({
            type: 'pins',
            newPins: []
          });
        });
  };

  useEffect(() => {
    if (mainState.filters.search && mainState.geo.sw_lat) {
      fetchResource();
      setPinsLoading(false);
    }
  }, [stringify(mainState.filters), stringify(mainState.geo)]);

};

export default useFetch;
