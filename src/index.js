import React, {lazy, Suspense} from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter, Switch, Route} from "react-router-dom";
import { Provider } from 'react-redux';
import { store, persistor} from "./redux/store";
import './styles/index.scss';
import * as serviceWorker from './serviceWorker';
import { PersistGate } from 'redux-persist/integration/react';
import ErrorBoundary from "./components/ErrorBoundary/ErrorBoundary";
import 'react-redux-toastr/lib/css/react-redux-toastr.min.css'
import ReduxToastr from 'react-redux-toastr'

import Spinner from "./components/Spinner/Spinner";
const HomePage = lazy(() => import('./pages/Home/HomePage'));
const ResultsPage = lazy(() => import('./pages/Results/ResultsPage'));
const MapPage = lazy(() => import('./pages/Map/Map'));
const NotFound = lazy(() => import('./components/NotFound/NotFound'));


const App = () => {
  return (
      <Provider store={store}>
        <BrowserRouter>
          <PersistGate persistor={persistor}>
              <ErrorBoundary>
                <Suspense fallback={<Spinner/>}>
                  <Switch>
                    <Route exact path={'/'} component={HomePage}/>
                    <Route path={'/map'} component={MapPage}/>
                    <Route path={'/s'} component={ResultsPage}/>
                    <Route path="*" component={NotFound}/>
                  </Switch>
                </Suspense>
              </ErrorBoundary>
          </PersistGate>
        </BrowserRouter>
        <ReduxToastr
          timeOut={4000}
          newestOnTop={false}
          preventDuplicates
          position="bottom-left"
          getState={(state) => state.toastr} // This is the default
          transitionIn="fadeIn"
          transitionOut="fadeOut"
          progressBar
          closeOnToastrClick/>
      </Provider>
  )
};


ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
