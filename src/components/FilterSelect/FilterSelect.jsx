import React, {useEffect} from 'react';
import {connect} from "react-redux";
import {createStructuredSelector} from "reselect";
import {
  selectFiltersCategories,
  selectFiltersCategory,
  selectFiltersSearch,
} from "../../redux/filters/filters.selectors";
import Autocomplete from "@material-ui/lab/Autocomplete";
import TextField from "@material-ui/core/TextField";
import CircularProgress from "@material-ui/core/CircularProgress";
import {fetchCategoriesAction, setCategoryAction} from "../../redux/filters/filters.actions";
import _ from 'lodash';
import './filterSelect.scss';


const FilterSelect = ({
                        filterOptions,
                        fieldId,
                        fieldName,
                        label,
                        fieldValue,
                        filtersSearch,
                        filersCategory,
                        filtersCategories,
                        fetchCategoriesAction,
                        setCategoryAction,
                      }) => {

  console.log('RENDER FilterSelect', filersCategory);
  //let fieldValue = '';
  let options = [];

  const handleChangeFilter = (fieldId) => (event, value) => {

    // if (fieldId === 'country' && event.target.name === 'country' && event.target.value) {
    //   dispatch({type: 'country', newFilter: event.target.value});
    //   dispatch({type: 'region', newFilter: ''});
    //   dispatch({type: 'city', newFilter: ''});
    // }
    // if (event.target.name === 'region' && event.target.value) {
    //   dispatch({type: 'region', newFilter: event.target.value});
    //   dispatch({type: 'city', newFilter: ''});
    // }
    // if (event.target.name === 'city') {
    //   dispatch({type: 'city', newFilter: event.target.value});
    // }
    if (fieldId === 'category' && value && value.name) {
      setCategoryAction(value.name);
    }
  };



  // useFetchAreas(filterOptions);
  // if (filterOptions ==='countries') { options = mainState.filterOptions.countries; }
  // if (filterOptions ==='regions') { options = mainState.filterOptions.regions; }
  // if (filterOptions ==='cities') { options = mainState.filterOptions.cities; }
  if (filterOptions === 'categories') { options = filtersCategories; }


  // if (mainState.filters.country && filterOptions ==='countries') { fieldValue = mainState.filters.country; }
  // if (mainState.filters.region && filterOptions ==='regions') { fieldValue = mainState.filters.region; }
  // if (mainState.filters.city && filterOptions ==='cities') { fieldValue = mainState.filters.city; }
  if (filersCategory && filterOptions === 'categories') { fieldValue = filersCategory; }

  // const inputLabel = React.useRef(0);
  // const [labelWidth, setLabelWidth] = React.useState(0);
  // useEffect(() => {
  //   setLabelWidth(inputLabel.current.offsetWidth);
  // }, []);



  const [open, setOpen] = React.useState(false);
  // let [options, setOptions] = React.useState([]);
  const loading = open && options.length === 0;

  React.useEffect(() => {
    // let active = true;
    //
    // if (!loading) {
    //   return undefined;
    // }

    //(async () => {
    fetchCategoriesAction(filtersSearch);

      //const categories = await response.json();

      // if (active) {
      //   setOptions(response);
      // }
    //})();

    // return () => {
    //   active = false;
    // };
  }, [filtersSearch]);

  // React.useEffect(() => {
  //   if (!open) {
  //     setOptions([]);
  //   }
  // }, [open]);





  options = options.map(option => {
    const firstLetter = _.capitalize(option.name[0]);
    return {
      firstLetter: /[0-9]/.test(firstLetter) ? '0-9' : firstLetter,
      ...option,
    };
  });

  const handleClear = () => {
    console.log('handleClear');
  };

  return (
    <Autocomplete
      id={fieldId}
      options={options.sort((a, b) => -b.firstLetter.localeCompare(a.firstLetter))}
      groupBy={option => option.firstLetter}
      getOptionLabel={option => _.startCase(option.name)}
      // getOptionSelected={(option, value) => option.name === value.name}
      // getOptionDisabled={option => option === timeSlots[0] || option === timeSlots[2]}
      style={{ width: '100%' }}
      size="small"
      open={open}
      onOpen={() => { setOpen(true); }}
      onClose={() => { setOpen(false); handleClear()}}
      //onInputChange={handleInputChangeFilter(fieldId)}
      onChange={handleChangeFilter(fieldId)}
      loading={loading}
      value={{name: fieldValue}}
      className={'filter-select'}
      renderInput={params => (
        <TextField {...params}
                   label={label}
                   variant="outlined"
                   fullWidth
                   InputProps={{
                     ...params.InputProps,
                     endAdornment: (
                       <React.Fragment>
                         {loading ? <CircularProgress color="inherit" size={20} /> : null}
                         {params.InputProps.endAdornment}
                       </React.Fragment>
                     ),
                   }}
        />
      )}
    />
    // {/*<FormControl variant="outlined" className={'filter-select'} >*/}
    //   {/*<InputLabel ref={inputLabel} htmlFor={'outlined'}>{fieldName}</InputLabel>*/}
    //   {/*<Select*/}
    //     {/*value={fieldValue}*/}
    //     {/*className={'select'}*/}
    //     {/*onChange={handleChangeFilter(fieldId)}*/}
    //     {/*input={<OutlinedInput labelWidth={labelWidth} name={fieldId} id={'outlined'}/>}*/}
    //   {/*>*/}
    //     {/*{options.map(option => {*/}
    //         {/*return <MenuItem key={option.id} value={option.name}>{_.upperFirst(option.name)}</MenuItem>*/}
    //     {/*})}*/}
    //   {/*</Select>*/}
    //   {/*/!*<FormHelperText>Error</FormHelperText>*!/*/}
    // {/*</FormControl>*/}
  )

};

const mapStateToProps = createStructuredSelector({
  filtersSearch: selectFiltersSearch,
  filersCategory: selectFiltersCategory,
  filtersCategories: selectFiltersCategories,
});

const mapActionsToProps = dispatch => ({
  fetchCategoriesAction: search => dispatch(fetchCategoriesAction(search)),
  setCategoryAction: category => dispatch(setCategoryAction(category)),
});

export default connect(mapStateToProps, mapActionsToProps)(FilterSelect);
