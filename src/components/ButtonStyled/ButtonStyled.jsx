import React from 'react';
import Button from "@material-ui/core/Button";
import {IconButton, withStyles} from "@material-ui/core";
import variables from '../../styles/variables.scss';
import './buttonStyled.scss';


const ButtonStyled = ({
                        variant='text',
                        color,
                        disabled=false,
                        href,
                        size,
                        type='button',
                        menuId,
                        edge,
                        ariaHaspopup,
                        ariaLabel,
                        startIcon,
                        className,
                        onClick,
                        children
}) => {

  let labelColor = variables.buttonTextColor;
  if (variant === 'text' && color === 'secondary' ||
    variant === 'outlined' && color === 'secondary') {
    labelColor = variables.buttonMainTextColor;
  }
  let bgColor = variables.backgroundColor;
  if (variant === 'contained' && color === 'secondary') {
    bgColor = variables.mainColor;
  }

  const ColorButton = withStyles(theme => ({
    root: {
      minWidth: 'auto',
      // color: theme.palette.getContrastText('#000'),
      backgroundColor: bgColor,
      boxShadow: `${variables.buttonShadowLeft} ${variables.buttonShadowTop} ${variables.buttonShadowBlur} ${variables.buttonShadowRightBottomColor},` +
        `${variables.buttonShadowRight} ${variables.buttonShadowBottom} ${variables.buttonShadowBlur} ${variables.buttonShadowLeftTopColor} !important`,
      label: {
        color: labelColor,
      },
      '&:hover': {
        boxShadow:  'none !important',
      },
      '&:focus': {
        boxShadow: `inset ${variables.buttonShadowLeft} ${variables.buttonShadowTop} ${variables.buttonShadowBlur} ${variables.buttonShadowRightBottomColor},`+
          `inset ${variables.buttonShadowRight} ${variables.buttonShadowBottom} ${variables.buttonShadowBlur} ${variables.buttonShadowLeftTopColor} !important`,
      },
    },
    startIcon: {
      color: variables.buttonIconColor
    }
  }))(Button);


  if(type === 'icon') {
    return (
      <IconButton
        className={'icon-button'}
        aria-controls={menuId}
        edge={edge}
        aria-haspopup={ariaHaspopup}
        aria-label={ariaLabel}
        variant={variant}
        color={color}
        disabled={disabled}
        href={href}
        size={size}
        onClick={onClick}
      >
        {children}
      </IconButton>
    )
  } else {
    return (
      <ColorButton
        variant={variant}
        color={color}
        disabled={disabled}
        href={href}
        size={size}
        onClick={onClick}
        startIcon={startIcon}
        className={className}
      >
        {children}
      </ColorButton>
    )
  }
};

export default ButtonStyled;
