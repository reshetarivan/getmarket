import React from 'react';
import {IconButton, Zoom} from "@material-ui/core";
import variables from '../../styles/variables.scss';
import MenuItem from "@material-ui/core/MenuItem";
import Menu from "@material-ui/core/Menu";
import {AccountCircle} from "@material-ui/icons";
import './buttonAccount.scss';
import Tooltip from "@material-ui/core/Tooltip";

const ButtonAccount = () => {

  const [anchorEl, setAnchorEl] = React.useState(null);
  const isMenuOpen = Boolean(anchorEl);
  const menuId = 'primary-account-menu';

  const handleProfileMenuOpen = event => {
    setAnchorEl(event.currentTarget);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
  };

  return (
    <div className={'button-account'}>
        <IconButton
          className={'icon-button'}
          aria-controls={menuId}
          edge={'end'}
          aria-haspopup="true"
          aria-label={'account of current user'}
          onClick={handleProfileMenuOpen}
        >
          <Tooltip title="Profile" TransitionComponent={Zoom} placement="bottom" arrow>
            <AccountCircle />
          </Tooltip>
        </IconButton>
      <Menu
        anchorEl={anchorEl}
        anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
        id={menuId}
        keepMounted
        transformOrigin={{ vertical: 'top', horizontal: 'right' }}
        open={isMenuOpen}
        onClose={handleMenuClose}
      >
        <MenuItem onClick={handleMenuClose}>Profile</MenuItem>
        <MenuItem onClick={handleMenuClose}>My account</MenuItem>
      </Menu>
    </div>
  )
};

export default ButtonAccount;
