import React, {useState} from 'react';
import {Zoom} from "@material-ui/core";
import Avatar from "@material-ui/core/Avatar";
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import Tooltip from "@material-ui/core/Tooltip";
import MonetizationOnIcon from '@material-ui/icons/MonetizationOn';
import LocalShippingIcon from '@material-ui/icons/LocalShipping';
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from '@material-ui/icons/Delete';
import ButtonStyled from "../ButtonStyled/ButtonStyled";
import AttachMoneyIcon from '@material-ui/icons/AttachMoney';
import EditIcon from '@material-ui/icons/Edit';
import Box from "@material-ui/core/Box";
import './cart.scss';
import Backdrop from "@material-ui/core/Backdrop";


const Cart = () => {

  const [cartOpen, setCartOpen] = useState(false);

  const handleCartOpen = () => {
    setCartOpen(!cartOpen);
  };

  const cartItems = [{
    id: 1,
    name: 'iPhone 8 Pro',
    price: 145,
    deliveryCost: 23,
    amount: 1,
    img: 'iphone8.jpg'
  },
    {
      id: 2,
      name: 'Case for iPhone 8 Pro',
      price: 45,
      deliveryCost: 8,
      amount: 2,
      img: 'apple-cell-phone.jpg'
    }];

  return (
    <Box className={'cart-container'}>
      <Box className={`cart ${cartOpen ? 'open': ''}`}>
        <Box className={'cart-title'}>
          <ShoppingCartIcon className={'cart-title-icon'}/>
          Shopping Cart
        </Box>
        <Box className={'cart-body'}>
          <Box className={'cart-items-box'}>
            {cartItems ? cartItems.map(item => (
              <Box className={'cart-item'} key={item.id}>
                <Avatar
                  className={'avatar'}
                  alt={item.name}
                  src={`/img/items/${item.img}`}
                />
                <Box className={'cart-item-descr'}>
                  <Box className={'cart-item-title'}>{item.name}</Box>
                  <Box className={'cart-item-price-box'}>
                    <Box className={'cart-item-price'}>
                      <Tooltip title="Price" TransitionComponent={Zoom} placement="top-start" arrow>
                        <MonetizationOnIcon className={'price-icon'}/>
                      </Tooltip>
                      ${item.price}
                    </Box>
                    <Tooltip title="Amount" TransitionComponent={Zoom} placement="top-start" arrow>
                      <Box className={'cart-item-amount'}>
                        {`x${item.amount}`}
                      </Box>
                    </Tooltip>
                  </Box>
                  <Box className={'cart-item-delivery-box'}>
                    <Box className={'cart-item-delivery'}>
                      <Tooltip title="Delivery" TransitionComponent={Zoom} placement="top-start" arrow>
                        <LocalShippingIcon className={'delivery-icon'}/>
                      </Tooltip>
                      ${item.deliveryCost}
                    </Box>
                    <Box className={'cart-item-remove'}>
                      <Tooltip title="Remove" TransitionComponent={Zoom} placement="top-start" arrow>
                        <IconButton className={'remove-icon'} aria-label="remove" size="small">
                          <DeleteIcon/>
                        </IconButton>
                      </Tooltip>
                    </Box>
                  </Box>
                </Box>
              </Box>
            )) : (<Box>No items added</Box>)}
          </Box>
          <Box className={'total-box'}>
            <div>TOTAL:</div>
            <div className={'total-sum'}>$123</div>
          </Box>
        </Box>
        <Box className={'cart-footer'}>
          <ButtonStyled
            variant={'contained'}
            color={'secondary'}
            size={'small'}
            className={'buy-button'}
          >
            <AttachMoneyIcon className={'buy-button-icon'}/> Cash Desk
          </ButtonStyled>
        </Box>
      </Box>
      <Tooltip title="Open Shopping Cart" TransitionComponent={Zoom} placement="left" arrow>
        <Box className={`cart-mobile ${cartOpen ? 'open': ''}`} onClick={handleCartOpen}>
          <ShoppingCartIcon className={'cart-title-icon mobile'}/>
          <Box className={'cart-mobile-title'}>Cart</Box>
          <Box className={'cart-mobile-amount'}>2</Box>
          <Box className={'cart-mobile-items'}>items</Box>
          <Box className={'cart-mobile-price'}>$123</Box>
        </Box>
      </Tooltip>
      <Backdrop className={'backdrop'} open={cartOpen} onClick={handleCartOpen}> </Backdrop>
    </Box>
  )
};

export default Cart;
