import React from 'react';
import {List} from "@material-ui/core";
import Drawer from "@material-ui/core/Drawer";
import Divider from "@material-ui/core/Divider";
import Box from "@material-ui/core/Box";
import {InputSearch, FilterPanel} from "../";
import LinearProgress from "@material-ui/core/LinearProgress";
import './infoPanel.scss';
import ListPinItem from "../ListPinItem/ListPinItem";
import Counters from "../Counters/Counters";
import {setIsHoverAction} from "../../redux/pins/pins.actions";
import {connect} from "react-redux";
import SecondDrawer from "../SecondDrawer/SecondDrawer";
import {createStructuredSelector} from "reselect";
import Reviews from "../Reviews/Reviews";

const InfoPanel = ({ shops = {pins: {data: []}}, open, loading, setIsHoverAction}) => {

  const tempArray = [
    {shopId: "1", shopName: '', shopImg: '', items: []},
    {shopId: "2", shopName: '', shopImg: '', items: []},
    {shopId: "3", shopName: '', shopImg: '', items: []},
    {shopId: "4", shopName: '', shopImg: '', items: []},
    {shopId: "5", shopName: '', shopImg: '', items: []},
    {shopId: "6", shopName: '', shopImg: '', items: []},
  ];

  console.log('RENDER INFOPANEL', shops);

    return (
      <div className={'info-panel'}>
        <Drawer
          variant="persistent"
          anchor="left"
          open={open}
          classes={{
            root: 'drawer',
            paper: 'drawerPaper',
          }}
        >
          <Box className={'searchBox'}>
            <InputSearch isAutoFocus={false} />
          </Box>
          <FilterPanel className={'noPadding'}/>
          <Divider />
          <Counters shops={shops} loading={loading}/>
          <List className={'listOfItems'} onMouseLeave={()=>setIsHoverAction(null)}>
            { loading ? <LinearProgress color='secondary' /> : '' }
            { (loading ? tempArray : shops.pins?.data).map(shop => (
              <ListPinItem key={shop.shopId} shop={shop} />
            ))}
            { !loading && !shops.pins?.data?.length ? <div className={'no-results'}> No results found </div> : ''}
          </List>
          <Divider />
        </Drawer>
        <SecondDrawer>
          <Reviews/>
        </SecondDrawer>
      </div>
    );
  };

const mapStateToProps = createStructuredSelector({
  // shops: {},
  // shopsIsLoading: selectPinsIsLoading,
});

const mapActionsToProps = dispatch => ({
  setIsHoverAction: pin => dispatch(setIsHoverAction(pin)),
});

export default connect(mapStateToProps, mapActionsToProps)(InfoPanel);
