import React from 'react';
import './footer.scss';
import Box from "@material-ui/core/Box";
import Link from "@material-ui/core/Link";
import Typography from "@material-ui/core/Typography";

export default () => {

  console.log('RENDER FOOTER');

  return (
    <Box className={'footer'}>
      <Box className={'links-box'}>
        <Link href={'/terms'}>Terms</Link>
        <Link href={'/privacy'}>Privacy</Link>
        <Link href={'/for-sellers'}>For sellers</Link>
        <Link href={'/for-buyers'}>For buyers</Link>
        <Link href={'/contacts'}>Contacts</Link>
      </Box>
      <Box className={'copyright-box'}>
        <Typography>Copyright © 2020, Ivan Reshetar. All rights reserved.</Typography>
      </Box>
    </Box>
  )
}
