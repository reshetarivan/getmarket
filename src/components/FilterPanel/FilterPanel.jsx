import React from 'react';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ExpandLessIcon from '@material-ui/icons/ExpandLess';
import {FilterSelect, RangeSelect} from "../";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Radio from "@material-ui/core/Radio";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import {connect} from "react-redux";
import {createStructuredSelector} from "reselect";
import {
  selectFiltersAll,
  selectFiltersType
} from "../../redux/filters/filters.selectors";
import {clearFiltersAction, setTypeAction} from "../../redux/filters/filters.actions";
import './FilterPanel.scss';
import {startGetPinsAction} from "../../redux/pins/pins.actions";
import Link from "@material-ui/core/Link";
import ClearIcon from '@material-ui/icons/Clear';


const CustomizedExpansionPanels = ({
                                     filtersAll,
                                     filtersType,
                                     setTypeAction,
                                     startGetPinsAction,
                                     clearFiltersAction
}) => {

  const [expanded, setExpanded] = React.useState('');

  const countActiveFilters = () => {
    let activeFilters = Object.keys(filtersAll).filter(e =>
      e !== 'options' &&
      e !== 'minPrice' &&
      e !== 'maxPrice' &&
      e !== 'search' &&
      e !== 'suggestions' &&
      e !== 'isLoading' &&
      e !== 'isError' &&
      e !== 'searchImg'
    );
    activeFilters = Object.values(activeFilters).filter(v => filtersAll[v] );
    let totalActiveFilters = activeFilters.length;
    if (filtersAll['fromPrice'] && filtersAll['toPrice']) {
      totalActiveFilters = totalActiveFilters-1;
    }
    return totalActiveFilters
  };


  const handleChange = panel => (event, newExpanded) => {
    setExpanded(newExpanded ? panel : false);
  };

  const changeType = event => {
    setTypeAction(event.target.value);
  };

  const onClearFilters = () => {
    clearFiltersAction();
  };


  return (
    <div className={'filter-panel'}>
      <ExpansionPanel
        className={'expansion-panel'}
        square
        expanded={expanded === 'panel1'}
        onChange={handleChange('panel1')}
        TransitionProps={{ unmountOnExit: true }}
      >
        <ExpansionPanelSummary
          expandIcon={<ExpandMoreIcon className={'expand-icon'}/>}
          aria-controls="panel1d-content"
          id="panel1d-header"
          className={'expansion-panel-summary'}
        >
          <Typography>
            Active Filters:
            <span className={'active-filters'}>
              {countActiveFilters()}
            </span>
          </Typography>
          <Link>
            <Typography>{expanded ? 'Hide' : 'Show'} Filters</Typography>
          </Link>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails className={'expansion-panel-details'}>
          <RadioGroup
            aria-label="position"
            className={'radio-group'}
            name="type"
            value={filtersType}
            onChange={changeType}
            row
          >
            <FormControlLabel
              value="p"
              control={<Radio />}
              label="Product"
              labelPlacement="end"
            />
            <FormControlLabel
              value="s"
              control={<Radio />}
              label="Service"
              labelPlacement="end"
            />
          </RadioGroup>
          <FilterSelect filterOptions={'categories'} fieldId={'category'} fieldName={'Category'} />
          {/*<FilterSelect filterOptions={'countries'} fieldId={'country'} fieldName={'Country'} />*/}
          {/*<FilterSelect filterOptions={'regions'} fieldId={'region'} fieldName={'Region'} />*/}
          {/*<FilterSelect filterOptions={'cities'} fieldId={'city'} fieldName={'City'} />*/}
          <RangeSelect/>
        </ExpansionPanelDetails>
        <ExpansionPanelSummary
          expandIcon={<ExpandLessIcon onClick={handleChange('')} className={'expand-icon'} />}
          aria-controls="panel2d-content"
          id="panel2d-header"
          className={'expansion-panel-summary'}
        >
          <Link className={'clear-filters'} onClick={onClearFilters}>
            <Typography>Clear Filters</Typography>
            <ClearIcon />
          </Link>
          <Link>
            <Typography onClick={handleChange('')}>{expanded ? 'Hide' : 'Show'} Filters</Typography>
          </Link>
        </ExpansionPanelSummary>
      </ExpansionPanel>
    </div>
  );
};

const mapStateToProps = createStructuredSelector({
  filtersAll: selectFiltersAll,
  filtersType: selectFiltersType,
});

const mapActionsToProps = dispatch => ({
  setTypeAction: type => dispatch(setTypeAction(type)),
  startGetPinsAction: query => dispatch(startGetPinsAction(query)),
  clearFiltersAction: () => dispatch(clearFiltersAction()),
});

export default connect(mapStateToProps, mapActionsToProps)(CustomizedExpansionPanels);
