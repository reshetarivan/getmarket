import React from "react";
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import Fab from "@material-ui/core/Fab";
import './drawerHandle.scss';

export default ({open, setOpen}) => {

  function handleDrawerToggle() {
    if (open) {
      setOpen(false);
    } else {
      setOpen(true);
    }
  }

  return (
    <Fab
      onClick={handleDrawerToggle}
      size='medium'
      className={`drawer-button ${open ? 'opened' : ''}`}
    >
      {open ? <ChevronLeftIcon /> : <ChevronRightIcon />}
    </Fab>
  )
}
