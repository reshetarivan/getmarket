import React from 'react';
import {ExpandLess, ExpandMore} from "@material-ui/icons";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import Collapse from "@material-ui/core/Collapse";
import List from "@material-ui/core/List";
import MonetizationOnIcon from '@material-ui/icons/MonetizationOn';
import ListItem from "@material-ui/core/ListItem";

const FilterPrice = () => {
  const [isExpanded, setIsExpanded] = React.useState(false);
  const handleClick = () => {
    setIsExpanded(!isExpanded);
  };

  return (
    <React.Fragment>
      <ListItem button key={'price'} onClick={handleClick}>
        <ListItemIcon>
          <MonetizationOnIcon />
        </ListItemIcon>
        <ListItemText primary="Price" />
        {isExpanded ? <ExpandLess /> : <ExpandMore />}
      </ListItem>
      <Collapse in={isExpanded} timeout="auto" unmountOnExit>
        <List component="div" disablePadding className={'nested-list'}>
          Price
        </List>
      </Collapse>
    </React.Fragment>
  )
};

export default FilterPrice;
