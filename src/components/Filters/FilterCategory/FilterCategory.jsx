import React from 'react';
import {ExpandLess, ExpandMore} from "@material-ui/icons";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import Collapse from "@material-ui/core/Collapse";
import List from "@material-ui/core/List";
import LineWeightIcon from '@material-ui/icons/LineWeight';
import FilterSelect from "../../FilterSelect/FilterSelect";
import ListItem from "@material-ui/core/ListItem";

const FilterCategory = () => {
  const [isExpanded, setIsExpanded] = React.useState(false);
  const handleClick = () => {
    setIsExpanded(!isExpanded);
  };

  return (
    <React.Fragment>
      <ListItem button key={'location'} onClick={handleClick}>
        <ListItemIcon>
          <LineWeightIcon />
        </ListItemIcon>
        <ListItemText primary="Category" />
        {isExpanded ? <ExpandLess /> : <ExpandMore />}
      </ListItem>
      <Collapse in={isExpanded} timeout="auto" unmountOnExit>
        <List component="div" disablePadding className={'nested-list'}>
          <ListItem button className={'nested-item'}>
            <FilterSelect
              label={'Category'}
              filterHeight={'small'}
              // source={citiesUrl}
              filterOptions={'category'}
              fieldId={'category'}
              fieldName={'Category'}
              fieldValue={'Clothes'}
              // handleChangeFilter={handleChangeFilter}
            />
          </ListItem>
        </List>
      </Collapse>
    </React.Fragment>
  )
};

export default FilterCategory;
