import React from 'react';
import {ExpandLess, ExpandMore} from "@material-ui/icons";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import Collapse from "@material-ui/core/Collapse";
import List from "@material-ui/core/List";
import LocalShippingIcon from '@material-ui/icons/LocalShipping';
import ListItem from "@material-ui/core/ListItem";

const FilterDelivery = () => {
  const [isExpanded, setIsExpanded] = React.useState(false);
  const handleClick = () => {
    setIsExpanded(!isExpanded);
  };

  return (
    <React.Fragment>
      <ListItem button key={'delivery'} onClick={handleClick}>
        <ListItemIcon>
          <LocalShippingIcon />
        </ListItemIcon>
        <ListItemText primary="Delivery" />
        {isExpanded ? <ExpandLess /> : <ExpandMore />}
      </ListItem>
      <Collapse in={isExpanded} timeout="auto" unmountOnExit>
        <List component="div" disablePadding className={'nested-list'}>
          Delivery
        </List>
      </Collapse>
    </React.Fragment>
  )
};

export default FilterDelivery;
