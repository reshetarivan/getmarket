import React from 'react';
import {ExpandLess, ExpandMore} from "@material-ui/icons";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import Collapse from "@material-ui/core/Collapse";
import List from "@material-ui/core/List";
import PaletteIcon from '@material-ui/icons/Palette';
import ListItem from "@material-ui/core/ListItem";

const FilterColor = () => {
  const [isExpanded, setIsExpanded] = React.useState(false);
  const handleClick = () => {
    setIsExpanded(!isExpanded);
  };

  return (
    <React.Fragment>
      <ListItem button key={'color'} onClick={handleClick}>
        <ListItemIcon>
          <PaletteIcon />
        </ListItemIcon>
        <ListItemText primary="Color" />
        {isExpanded ? <ExpandLess /> : <ExpandMore />}
      </ListItem>
      <Collapse in={isExpanded} timeout="auto" unmountOnExit>
        <List component="div" disablePadding className={'nested-list'}>
          Color
        </List>
      </Collapse>
    </React.Fragment>
  )
};

export default FilterColor;
