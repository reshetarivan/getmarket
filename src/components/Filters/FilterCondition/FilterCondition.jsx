import React from 'react';
import {ExpandLess, ExpandMore} from "@material-ui/icons";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import Collapse from "@material-ui/core/Collapse";
import List from "@material-ui/core/List";
import Battery80Icon from '@material-ui/icons/Battery80';
import ListItem from "@material-ui/core/ListItem";

const FilterCondition = () => {
  const [isExpanded, setIsExpanded] = React.useState(false);
  const handleClick = () => {
    setIsExpanded(!isExpanded);
  };

  return (
    <React.Fragment>
      <ListItem button key={'condition'} onClick={handleClick}>
        <ListItemIcon>
          <Battery80Icon />
        </ListItemIcon>
        <ListItemText primary="Condition" />
        {isExpanded ? <ExpandLess /> : <ExpandMore />}
      </ListItem>
      <Collapse in={isExpanded} timeout="auto" unmountOnExit>
        <List component="div" disablePadding className={'nested-list'}>
          Condition
        </List>
      </Collapse>
    </React.Fragment>
  )
};

export default FilterCondition;
