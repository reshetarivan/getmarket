import React from 'react';
import {ExpandLess, ExpandMore} from "@material-ui/icons";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import Collapse from "@material-ui/core/Collapse";
import List from "@material-ui/core/List";
import LocationCityIcon from '@material-ui/icons/LocationCity';
import FilterSelect from "../../FilterSelect/FilterSelect";
import ListItem from "@material-ui/core/ListItem";

const FilterLocation = () => {
  const [isExpanded, setIsExpanded] = React.useState(false);
  const handleClick = () => {
    setIsExpanded(!isExpanded);
  };

  return (
    <React.Fragment>
      <ListItem button key={'location'} onClick={handleClick}>
        <ListItemIcon>
          <LocationCityIcon />
        </ListItemIcon>
        <ListItemText primary="Location" />
        {isExpanded ? <ExpandLess /> : <ExpandMore />}
      </ListItem>
      <Collapse in={isExpanded} timeout="auto" unmountOnExit>
        <List component="div" disablePadding className={'nested-list'}>
          <ListItem button className={'nested-item'}>
            <FilterSelect
              label={'Province'}
              filterHeight={'small'}
              // source={citiesUrl}
              filterOptions={'provinces'}
              fieldId={'province'}
              fieldName={'Province'}
              fieldValue={'Ontario'}
              // handleChangeFilter={handleChangeFilter}
            />
          </ListItem>
          <ListItem button className={'nested-item'}>
            <FilterSelect
              label={'City'}
              filterHeight={'small'}
              // source={citiesUrl}
              filterOptions={'cities'}
              fieldId={'city'}
              fieldName={'City'}
              fieldValue={'Toronto'}
              // handleChangeFilter={handleChangeFilter}
            />
          </ListItem>
        </List>
      </Collapse>
    </React.Fragment>
  )
};

export default FilterLocation;
