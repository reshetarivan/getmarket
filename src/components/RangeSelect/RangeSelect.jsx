import React from 'react';
import Slider from "@material-ui/core/Slider";
import {createStructuredSelector} from "reselect";
import {selectFiltersAll} from "../../redux/filters/filters.selectors";
import {connect} from "react-redux";
import {selectPinsAll} from "../../redux/pins/pins.selectors";
import {updatePriceRangeAction} from "../../redux/filters/filters.actions";
import './rangeSelect.scss';


const RangeSelect = ({pins, filters, updatePriceRangeAction}) => {

  const handleRange = (event, newValue) => {
    const priceRange = {
      // minPrice: parseFloat(filters.minPrice),
      // maxPrice: parseFloat(filters.maxPrice),
      fromPrice: parseFloat(newValue[0]),
      toPrice: parseFloat(newValue[1]),
    };
    updatePriceRangeAction(priceRange)
  };

  return (
    <div className={'range-select'}>
      <Slider
        value={[filters.fromPrice, filters.toPrice]}
        onChange={handleRange}
        valueLabelDisplay="on"
        max={filters.maxPrice}
        min={filters.minPrice}
      />
    </div>
  )

};

const mapStateToProps = createStructuredSelector({
  pins: selectPinsAll,
  filters: selectFiltersAll
});

const mapDispatchToProps = dispatch => ({
  updatePriceRangeAction: priceRange => dispatch(updatePriceRangeAction(priceRange))
});

export default connect(mapStateToProps, mapDispatchToProps)(RangeSelect);
