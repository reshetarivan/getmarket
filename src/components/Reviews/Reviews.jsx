import React, {useEffect, useState} from 'react';
import IconButton from "@material-ui/core/IconButton";
import ArrowBackRoundedIcon from '@material-ui/icons/ArrowBackRounded';
import Box from "@material-ui/core/Box";
import './reviews.scss';
import _ from 'lodash';
import {addReviewAction, setIsOpenAction, startGetReviewsAction} from "../../redux/reviews/reviews.actions";
import {connect} from "react-redux";
import FavoriteIcon from '@material-ui/icons/Favorite';
import Rating from "@material-ui/lab/Rating";
import Slider from "@material-ui/core/Slider";
import withStyles from "@material-ui/core/styles/withStyles";
import variables from '../../styles/variables.scss';
import Divider from "@material-ui/core/Divider";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import Avatar from "@material-ui/core/Avatar";
import ListItemText from "@material-ui/core/ListItemText";
import Typography from "@material-ui/core/Typography";
import ButtonStyled from "../ButtonStyled/ButtonStyled";
import TextField from "@material-ui/core/TextField";
import {createStructuredSelector} from "reselect";
import {selectReviewsAll, selectShopReview} from "../../redux/reviews/reviews.selectors";
import Skeleton from "@material-ui/lab/Skeleton";
import Moment from 'react-moment';
import CircularProgress from "@material-ui/core/CircularProgress";



const Reviews = ({shop, setIsOpenAction, selectReviewsAll, selectShopReview, startGetReviewsAction, addReviewAction}) => {

  console.log('RENDER REVIEWS');
  const userId = 6;
  const maxReviewEnterLength = 1000;
  const [reviewId, setReviewId] = useState(0);
  const [isFormOpen, setFormOpen] = useState(false);
  const [formButtonName, setFormButtonName] = useState('+ Add review');
  const [isButtonActive, setButtonActive] = useState(true);
  const [myReview, setMyReview] = useState('');
  const [myRating, setMyRating] = useState(0);

  const findMyReview = () => {
    const isMyReview = selectReviewsAll.data.find(review => parseInt(review.user_id) === userId );
    if (isMyReview) {
      setReviewId(isMyReview.id);
      setMyReview(isMyReview.review);
      setMyRating(parseInt(isMyReview.rate));
      setButtonActive(true);
      setFormButtonName('Update review');
    } else {
      setReviewId(0);
      setMyReview('');
      setMyRating(0);
      setButtonActive(true);
      setFormButtonName('+ Add review');
    }
  };

  useEffect(()=>{
    startGetReviewsAction();
  },[selectShopReview]);

  useEffect(()=>{
    findMyReview();
  },[selectReviewsAll.data]);

  const onClickBack = () => {
    setIsOpenAction(false, selectShopReview);
  };

  const onReviewChange = (e) => {
    if (e.target.value.length <= maxReviewEnterLength) {
      setMyReview(e.target.value);
    }
  };

  const onRatingChange = (e) => {
    setMyRating(parseInt(e.target.value));
    setButtonActive(true);
  };

  const handleSubmit = async () => {

    if (!isFormOpen) {
      if (!myRating) { setButtonActive(false); }
      setFormOpen(true);
      setFormButtonName('Submit review');
    } else {
      const formData = {
        id: reviewId,
        shop_id: parseInt(selectShopReview.shopId),
        user_id: userId,
        rate: myRating,
        review: myReview,
        date: Math.round(new Date() / 1000)
      };
      await addReviewAction(formData);
      // if (selectReviewsAll.isSuccess) {
      //   setMyReview('');
      //   setMyRating(0);
        setFormOpen(false);
        setFormButtonName('Update review');
        setButtonActive(true);
      // }
    }
  };

  const PrettoSlider = withStyles({
    root: {
      height: 2,
      margin: 0,
      padding: '10px 0',
      width: '100%'
    },
    thumb: {
      display: 'none'
    },
    track: {
      height: 6,
      borderRadius: 2,
    },
    rail: {
      height: 6,
      borderRadius: 2,
      color: variables.gray
    },
  })(Slider);


  if (selectShopReview && selectReviewsAll) {
    return (
      <Box className={'reviews'}>
        <Box  className={'reviews-header'}>
          <IconButton aria-label="back" onClick={onClickBack} className={'back-button'} >
            <ArrowBackRoundedIcon/>
          </IconButton>
          <div className={'reviews-title'}>
            Reviews
          </div>
        </Box>
        <Divider/>
        <Box className={'review-container'}>
          <Box className={'rating-body'}>
            <Box className={'rating-value-box'}>
              { selectReviewsAll.isLoading ? (
                <Box className={'rating-value-skeleton'}>
                  <Skeleton width="100%" height="100%"/>
                </Box>
              ) : (
                <Box className={'rating-value'}>{selectReviewsAll.rating}</Box>
              )}
              { selectReviewsAll.isLoading ? (
                <Box className={'rating-rate-skeleton'}>
                  <Skeleton width="100%"/>
                  <Skeleton width="30%" style={{marginLeft: '28px'}}/>
                  <Skeleton width="70%" style={{marginLeft: '13px'}}/>
                </Box>
              ) : (
                <Box className={'rating-rate'}>
                  <Rating
                    name="size-small"
                    value={parseFloat(selectReviewsAll.rating)}
                    max={5}
                    precision={0.1}
                    size="small"
                    className={'rating'}
                    readOnly
                    icon={<FavoriteIcon fontSize="inherit" />}
                  />
                  <div className={'rating-counter'}>{selectReviewsAll.total}</div>
                  <div className={'rating-descr'}>reviews</div>
                </Box>
              )}
            </Box>
            <table border="0" className={'rating-sliders-box'}>
              <tbody>
                {selectReviewsAll.rates.map(r =>
                  selectReviewsAll.isLoading ? (
                  <tr className={'rating-slider'} key={r.rate}>
                    <td>
                      <Skeleton width="100%" height={'25px'}/>
                    </td>
                  </tr>
                ) : (
                  <tr className={'rating-slider'} key={r.rate}>
                    <td className={'rating-slider-num'}>{r.rate}</td>
                    <td className={'rating-slider-body'}>
                      <PrettoSlider
                        valueLabelDisplay="on"
                        defaultValue={r.val ? parseInt((r.val/selectReviewsAll.total)*100) : 0}
                        disabled/>
                    </td>
                    <td className={'rating-slider-reviews'}>({r.val})</td>
                  </tr>
                ))}
              </tbody>
            </table>
          </Box>
          <Divider/>
          {selectReviewsAll.isLoading ? (
            <Box className={'add-review-box'}>
              <Skeleton width="50%" height={'30px'}/>
              <Skeleton width="50%" height={'55px'}/>
            </Box>
          ) : (
            <Box className={'add-review-box'}>
              <Typography className={'add-review-title'}>
                {selectShopReview.shopName}
              </Typography>
              {isFormOpen ? (
                <React.Fragment>
                  <Rating
                    name="add-review-rating"
                    value={myRating}
                    max={5}
                    precision={1}
                    className={'add-review-rating'}
                    icon={<FavoriteIcon fontSize="inherit" />}
                    onChange={onRatingChange}
                  />
                  <TextField
                    name="review-text"
                    label="My review"
                    fullWidth
                    multiline
                    rows="4"
                    variant="outlined"
                    className={(myReview.length >= maxReviewEnterLength) ? 'add-review-text exhausted' : 'add-review-text'}
                    value={myReview}
                    onChange={onReviewChange}
                  />
                  <Typography className={(myReview.length >= maxReviewEnterLength) ? 'text-counter exhausted' : 'text-counter'}>
                    {myReview.length}/{maxReviewEnterLength}
                  </Typography>
                </React.Fragment>
                ) : ''}
              <ButtonStyled
                className={'add-review-button'}
                variant="contained"
                color={'secondary'}
                onClick={handleSubmit}
                disabled={!isButtonActive}
              >
                {selectReviewsAll.isLoading ? <CircularProgress /> : formButtonName }
              </ButtonStyled>
            </Box>
          )}
          <Divider />
          <Box className={'reviews-list-box'}>
            <List className={'reviews-list'}>
              { selectReviewsAll.isLoading ? (
                  <Box>
                    <Skeleton width="100%" height={'100px'}/>
                    <Skeleton width="100%" height={'100px'}/>
                    <Skeleton width="100%" height={'100px'}/>
                  </Box>
                ) :
                  selectReviewsAll.data.length ? selectReviewsAll.data.map(review => (
                <Box key={review.id}>
                  <ListItem alignItems="flex-start">
                    <ListItemAvatar>
                      <Avatar alt="Remy Sharp" src="https://cdn2.iconfinder.com/data/icons/ios-7-icons/50/user_male2-512.png" />
                    </ListItemAvatar>
                    <ListItemText className={'review-body'}>
                      <Box>
                        <Typography className={'review-rating-title'}>
                          {_.capitalize(review.name)}
                        </Typography>
                      </Box>
                      <Box className={'review-rating-box'}>
                        <Rating
                          name="review-rating"
                          value={parseInt(review.rate)}
                          max={5}
                          precision={0.1}
                          size="small"
                          className={'review-rating'}
                          readOnly
                          icon={<FavoriteIcon fontSize="inherit" />}
                        />
                        <Typography className={'review-rating-date'}>
                          <Moment fromNow>{new Date(parseInt(review.date)*1000)}</Moment>
                        </Typography>
                      </Box>
                      <Box>
                        <Typography
                          component="div"
                          variant="body2"
                          color="textPrimary"
                        >
                          {review.review}
                        </Typography>
                      </Box>
                      <Box className={'review-read-more-box'}>
                        <ButtonStyled variant="text" color="secondary" href="#text-buttons">Read more</ButtonStyled>
                      </Box>
                    </ListItemText>
                  </ListItem>
                  <Divider/>
                </Box>
                  )) : (<Box className={'reviews-not-found'}>No reviews found</Box>)}
            </List>
          </Box>
        </Box>
      </Box>
    )

  } else {

    return (
      <Box>
        No reviews found
      </Box>
    )
  }

};

const mapStateToProps = createStructuredSelector({
  selectShopReview,
  selectReviewsAll
});

const mapDispatchToProps = dispatch => ({
  setIsOpenAction: (isOpen, shop) => dispatch(setIsOpenAction(isOpen, shop)),
  startGetReviewsAction: () => dispatch(startGetReviewsAction()),
  addReviewAction: formData => dispatch(addReviewAction(formData))
});

export default connect(mapStateToProps, mapDispatchToProps)(Reviews);
