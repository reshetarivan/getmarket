import React from 'react';
import {Tooltip} from "@material-ui/core";
import RoomTwoToneIcon from '@material-ui/icons/RoomTwoTone';
import EmojiPeopleIcon from '@material-ui/icons/EmojiPeople';
import variables from '../../styles/variables.scss';
import './marker.scss';
import {createStructuredSelector} from "reselect";
import {selectPinsIsHover, selectSelectedPins} from "../../redux/pins/pins.selectors";
import {connect} from "react-redux";
import {selectPinAction} from "../../redux/pins/pins.actions";

const Marker = ({shop, isUserMarker=false, selectedPins, selectPinsIsHover, selectPinAction}) => {

  const checkIfPinIsSelected = () => {
      if (!!(selectedPins.find((pinItem) => pinItem.shopId === shop.shopId))) {
        return variables.mainColor;
      } else {
        return variables.blueGray;
      }
  };

  const renderPinIcon = (isUserMarker) => {
    if (isUserMarker) {
      return <EmojiPeopleIcon className={'pin'} htmlColor={variables.mainColor}/>;
    } else {
      return <RoomTwoToneIcon
        className={selectPinsIsHover === shop.shopId ? 'pin is-hover' : 'pin'}
        htmlColor={checkIfPinIsSelected()}
        onClick={()=>selectPinAction(shop)}
      />
    }
  };

  return (
    <div>
      <Tooltip
        title={
          <React.Fragment>
            <img
                className={'avatar'}
                alt={shop.shopName}
                src={`/img/shops/${shop.shopImg}`}
              />
              <div className={'tooltipComment'}>
                <div className={'tooltipHeader'}>{shop.shopName}</div>
                <div>It's very engaging. Right?</div>
              </div>
          </React.Fragment>
        }
        placement="top-start"
        interactive
        //open={true}
      >
        {renderPinIcon(isUserMarker)}
      </Tooltip>
      {selectPinsIsHover === shop.shopId || isUserMarker ? <span className={'beacon'}> </span> : ''}
    </div>
  );

};

const mapStateToProps = createStructuredSelector({
  selectedPins: selectSelectedPins,
  selectPinsIsHover: selectPinsIsHover
});

const mapActionsToProps = dispatch => ({
  selectPinAction: pin => dispatch(selectPinAction(pin)),
});

export default connect(mapStateToProps, mapActionsToProps)(Marker);
