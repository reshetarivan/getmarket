import React from 'react';
import Avatar from "@material-ui/core/Avatar";
import Skeleton from "@material-ui/lab/Skeleton";
import ListItemText from "@material-ui/core/ListItemText";
import Typography from "@material-ui/core/Typography";
import Rating from "@material-ui/lab/Rating";
import FavoriteIcon from '@material-ui/icons/Favorite';
import clsx from 'clsx';
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import ListItem from "@material-ui/core/ListItem";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import Checkbox from "../Checkbox/Checkbox";
import './listPinItem.scss';
import {connect} from "react-redux";
import {setIsHoverAction} from "../../redux/pins/pins.actions";
import _ from "lodash";
import Card from "@material-ui/core/Card";
import LocalOfferIcon from '@material-ui/icons/LocalOffer';
import MonetizationOnIcon from '@material-ui/icons/MonetizationOn';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Box from "@material-ui/core/Box";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import AccessTimeIcon from '@material-ui/icons/AccessTime';
import LanguageIcon from '@material-ui/icons/Language';
import PhoneIcon from '@material-ui/icons/Phone';
import Collapse from "@material-ui/core/Collapse";
import CardContent from "@material-ui/core/CardContent";
import {makeStyles} from "@material-ui/core";
import Link from "@material-ui/core/Link";
import CardActions from "@material-ui/core/CardActions";
import Button from "@material-ui/core/Button";
import AddShoppingCartIcon from '@material-ui/icons/AddShoppingCart';
import FavoriteBorderIcon from '@material-ui/icons/FavoriteBorder';
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import {setIsOpenAction} from "../../redux/reviews/reviews.actions";


const useStyles = makeStyles(theme => ({
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    marginTop: '-4px',
    cursor: 'pointer',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
}));

const ListPinItem = ({shop, setIsHoverAction, setIsOpenAction}) => {

  console.log('RENDER LIST PIN ITEM', shop);

  const classes = useStyles();
  const [expanded, setExpanded] = React.useState(false);
  const [value, setValue] = React.useState(0);

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  const handleTabChange = (event, newValue) => {
    setValue(newValue);
  };

  function a11yProps(index) {
    return {
      id: `scrollable-force-tab-${index}`,
      'aria-controls': `scrollable-force-tabpanel-${index}`,
    };
  }

  const schedule = [
    {day: 'Monday', open: '9am', close: '5pm', works: true},
    {day: 'Tuesday', open: '9am', close: '5pm', works: true},
    {day: 'Wednesday', open: '9am', close: '5pm', works: true},
    {day: 'Thursday', open: '9am', close: '5pm', works: true},
    {day: 'Friday', open: '9am', close: '5pm', works: true},
    {day: 'Saturday', open: '9am', close: '5pm', works: true},
    {day: 'Sunday', open: '', close: '', works: false},
  ];

  let minItemPrice = 0;
  let maxItemPrice = 0;
  if  (shop?.items?.length) {
      let minPriceObj = _.minBy(shop.items, e => parseFloat(e.pinPrice) > 0 ? parseFloat(e.pinPrice) : {});
      let maxPriceObj = _.maxBy(shop.items, e => parseFloat(e.pinPrice));

      if (parseFloat(minPriceObj.pinPrice)) {
        minItemPrice = parseFloat(minPriceObj.pinPrice);
      }
      if (parseFloat(maxPriceObj.pinPrice) > maxItemPrice) {
        maxItemPrice = parseFloat(maxPriceObj.pinPrice);
      }
  }


  if (shop) {
    return (

      <ExpansionPanel key={shop?.shopId} className={'item-panel'}>
        <ExpansionPanelSummary
          expandIcon={<ExpandMoreIcon/>}
          aria-label="Expand"
          aria-controls={`additional-actions${shop?.shopId}-content`}
          id={`additional-actions${shop?.shopId}-header`}
          className={'panel-summary'}
        >
          <FormControlLabel
            aria-label="Acknowledge"
            onClick={event => (event.preventDefault(), event.stopPropagation())}
            //onFocus={event => (event.preventDefault(), event.stopPropagation())}
            className={'item-body'}
            control={<Checkbox pin={shop} action={'selectPin'} title={'Select'}/>}
            label={
              <Box
                key={shop.shopId}
                // button
                // divider
                className={'list-item'}
                onMouseEnter={() => setIsHoverAction(shop.shopId)}
              >
                {shop.shopImg ? (
                  <ListItemAvatar className={'avatar-block'}>
                    <Box>
                      <Avatar
                        className={'avatar'}
                        alt={`${shop.shopName}`}
                        src={`/img/shops/${shop.shopImg}`}
                      />
                      <Card className={'items-block'} elevation={0}>
                        <div className={'items-count'}>{shop.items?.length}</div>
                        <div className={'items-found'}>{shop.items?.length > 1 ? 'results' : 'result'}</div>
                      </Card>
                    </Box>
                  </ListItemAvatar>
                ) : (
                  <ListItemAvatar className={'avatar-block'}>
                    <Box>
                      <Skeleton variant="rect" className={'avatar'}/>
                      <Card>
                        <Skeleton variant="rect" className={'avatar'}/>
                      </Card>
                    </Box>
                  </ListItemAvatar>
                )}

                {shop.shopName ? (
                  <Box className={'list-item-text'}>
                    <Typography className={'item-title'}>{shop.shopName}</Typography>
                    <Typography className={"item-descr"}>
                      {
                        `${shop.shopAddr ? shop.shopAddr + ', ' : ''}
                      ${_.startCase(shop.cityName)},
                      ${_.startCase(shop.regionName)}
                      ${shop.shopZipCode ? ', ' + shop.shopZipCode : ''}`
                      }
                    </Typography>
                    <Box className={'rating-block'} onClick={() => setIsOpenAction(true, shop)}>
                      <div className={'rating-number'}>{shop.shopRating}</div>
                      <Rating
                        name="size-small"
                        value={parseFloat(shop.shopRating)}
                        max={5}
                        size="small"
                        className={'rating'}
                        readOnly
                        precision={0.1}
                        icon={<FavoriteIcon fontSize="inherit"/>}
                      />
                      <div className={'rating-counter'}>
                        (<Link>{shop.shopReviews}</Link> review{shop.shopReviews > 1 ? 's' : ''})
                      </div>
                    </Box>
                    <Box className={'schedule-open-block'}>
                      <div className={'open-hours'}>Open now. Closes at 5.00pm. (4h:24m)</div>
                    </Box>
                    <Box className={'prices-discounts-box'}>
                      {minItemPrice || maxItemPrice ? (
                        <Card className={'prices-block'} title={'Prices'} elevation={0}>
                          <MonetizationOnIcon title={'Prices'} className={'prices-icon'}/>
                          <div className={'prices'}>
                            ${minItemPrice ? minItemPrice : ''}
                            {maxItemPrice && minItemPrice && minItemPrice !== maxItemPrice ? '-' : ''}
                            {maxItemPrice && minItemPrice !== maxItemPrice ? maxItemPrice : ''}
                          </div>
                        </Card>
                      ) : <Card/>}
                      <Card className={'discounts-block'} title={'Discounts'} elevation={0}>
                        <LocalOfferIcon className={'discounts-icon'}/>
                        <div className={'discounts'}>-20-50%</div>
                      </Card>
                    </Box>
                  </Box>
                ) : (
                  <Box className={'list-item-text'}>
                    <Skeleton width="100%"/>
                    <Skeleton width="80%"/>
                    <Skeleton width="60%"/>
                    <Box className={'prices-discounts-box'}>
                      <Skeleton width="30%"/>
                      <Skeleton width="30%"/>
                    </Box>
                  </Box>
                )}
              </Box>
            }
          >
          </FormControlLabel>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails className={'expansion-panel-summary'}>
          <Box className={'exp-panel-box'}>
            <AccessTimeIcon className={'exp-icon'}/>
            <Typography className={'exp-text'} onClick={handleExpandClick}>Open now. Closes at 5.00pm
              (4h:24m)</Typography>
            <ExpandMoreIcon
              className={clsx(classes.expand, {[classes.expandOpen]: expanded})}
              onClick={handleExpandClick}
              aria-expanded={expanded}
            />
          </Box>
          <Collapse in={expanded} timeout="auto" unmountOnExit className={'schedule-collapse'}>
            {schedule.map(sched => (
              <Box className={'schedule-day'} key={sched.day}>
                <Typography className={'schedule-day--name'}>{sched.day}</Typography>
                <Typography className={'schedule-day--time'}>
                  {!sched.works ? 'Closed' : sched.open + ' - ' + sched.close}
                </Typography>
              </Box>
            ))}
          </Collapse>
          <Box className={'exp-panel-box'}>
            <LanguageIcon className={'exp-icon'}/>
            <Link href={'https://website.com'} target="_blank" rel="noopener"
                  className={'exp-text'}>www.website.com</Link>
          </Box>
          <Box className={'exp-panel-box'}>
            <PhoneIcon className={'exp-icon'}/>
            <Link href={'tel:+1 (123) 456-78-90'} target="_blank" rel="noopener" className={'exp-text'}>+1 (123)
              456-78-90</Link>
          </Box>
          <Box>
            <Typography className={'results'}>Results ({shop?.items?.length}):</Typography>
          </Box>
          <Box className={'results-box'}>
            {shop.items ? shop.items.map(item => {
              let itemPrice = parseFloat(item.pinPrice);
              let discountedPrice = 0;
              let saved = 0;
              if (item.pinDiscount) {
                discountedPrice = parseFloat(itemPrice - ((itemPrice * item.pinDiscount) / 100));
                saved = itemPrice - discountedPrice;
              }

              return (
                <Card className={'result-card'} key={item.itemId}>
                  <CardContent>
                    <Typography className={'result-title'}>
                      {_.startCase(item.itemName)}
                    </Typography>
                    <Box className={'result-param-box'}>
                      <Typography
                        className={'result-param'}>Price: <span>${discountedPrice ? discountedPrice : itemPrice}-{discountedPrice}- {item.pinDiscount}%</span></Typography>
                      <Typography className={'result-param'}>Size: <span>M</span></Typography>
                    </Box>
                    <Box className={'result-param-box'}>
                      <Typography
                        className={'result-param'}>Quantity: <span>{item.pinQuantity ? item.pinQuantity : 'No  info'}</span></Typography>
                      <Typography className={'result-param'}>Color: <span>Black</span></Typography>
                    </Box>
                    <Box className={'result-param-box'}>
                      <Typography className={'result-param'}>Special
                        offer: <span>Buy 2 - get 1 for free</span></Typography>
                    </Box>
                    <Box>
                      <Tabs
                        value={value}
                        onChange={handleTabChange}
                        variant="scrollable"
                        scrollButtons="on"
                        indicatorColor="primary"
                        textColor="primary"
                        aria-label="scrollable force tabs example"
                      >
                        <Tab label={
                          <div>TEST</div>
                        } {...a11yProps(0)} />
                        <Tab label="Item Two" icon={<LanguageIcon/>} {...a11yProps(1)} />
                        <Tab label="Item Three" icon={<LanguageIcon/>} {...a11yProps(2)} />
                        <Tab label="Item Four" icon={<LanguageIcon/>} {...a11yProps(3)} />
                        <Tab label="Item Five" icon={<LanguageIcon/>} {...a11yProps(4)} />
                        <Tab label="Item Six" icon={<LanguageIcon/>} {...a11yProps(5)} />
                        <Tab label="Item Seven" icon={<LanguageIcon/>} {...a11yProps(6)} />
                      </Tabs>
                    </Box>
                  </CardContent>
                  <CardActions className={'result-actions'}>
                    <Button size="small" color="default" variant="contained">
                      <FavoriteBorderIcon className={'heart-icon'}/> Save to wishlist
                    </Button>
                    <Button size="small" color="secondary" variant="contained">
                      <AddShoppingCartIcon className={'cart-icon'}/> Add to cart
                    </Button>
                  </CardActions>
                </Card>
              )
            }) : ''
            }
          </Box>
          {/*<Card className={'result-card'}>*/}
          {/*<Tabs*/}
          {/*value={value}*/}
          {/*onChange={handleTabChange}*/}
          {/*variant="scrollable"*/}
          {/*scrollButtons="on"*/}
          {/*indicatorColor="secondary"*/}
          {/*textColor="secondary"*/}
          {/*className={'result-tabs'}*/}
          {/*>*/}
          {/*<Tab className={'result-tab'} label={*/}
          {/*<Card className={'result-card'}>*/}
          {/*<CardContent className={'result-card-content'}>*/}
          {/*<Typography className={'result-title'}>*/}
          {/*Item name*/}
          {/*</Typography>*/}
          {/*<Box className={'result-param-box'}>*/}
          {/*<Typography className={'result-param'}>Price: <span>$30</span></Typography>*/}
          {/*<Typography className={'result-param'}>Size: <span>M</span></Typography>*/}
          {/*</Box>*/}
          {/*<Box className={'result-param-box'}>*/}
          {/*<Typography className={'result-param'}>Quantity: <span>125</span></Typography>*/}
          {/*<Typography className={'result-param'}>Color: <span>Black</span></Typography>*/}
          {/*</Box>*/}
          {/*<Box className={'result-param-box'}>*/}
          {/*<Typography className={'result-param'}>Special offer: <span>Buy 2 - get 1 for free</span></Typography>*/}
          {/*</Box>*/}
          {/*</CardContent>*/}
          {/*<CardActions className={'result-actions'}>*/}
          {/*<Button size="small" color="default" variant="contained">*/}
          {/*<FavoriteBorderIcon/> Save to wishlist*/}
          {/*</Button>*/}
          {/*<Button size="small" color="secondary" variant="contained">*/}
          {/*<AddShoppingCartIcon /> Add to cart*/}
          {/*</Button>*/}
          {/*</CardActions>*/}
          {/*</Card>*/}
          {/*} {...a11yProps(0)} />*/}
          {/*<Tab className={'result-tab'} label={*/}
          {/*<Card className={'result-card'}>*/}
          {/*<CardContent className={'result-card-content'}>*/}
          {/*<Typography className={'result-title'}>*/}
          {/*Item name*/}
          {/*</Typography>*/}
          {/*<Box className={'result-param-box'}>*/}
          {/*<Typography className={'result-param'}>Price: <span>$30</span></Typography>*/}
          {/*<Typography className={'result-param'}>Size: <span>M</span></Typography>*/}
          {/*</Box>*/}
          {/*<Box className={'result-param-box'}>*/}
          {/*<Typography className={'result-param'}>Quantity: <span>125</span></Typography>*/}
          {/*<Typography className={'result-param'}>Color: <span>Black</span></Typography>*/}
          {/*</Box>*/}
          {/*<Box className={'result-param-box'}>*/}
          {/*<Typography className={'result-param'}>Special offer: <span>Buy 2 - get 1 for free</span></Typography>*/}
          {/*</Box>*/}
          {/*</CardContent>*/}
          {/*<CardActions className={'result-actions'}>*/}
          {/*<Button size="small" color="default" variant="contained">*/}
          {/*<FavoriteBorderIcon/> Save to wishlist*/}
          {/*</Button>*/}
          {/*<Button size="small" color="secondary" variant="contained">*/}
          {/*<AddShoppingCartIcon /> Add to cart*/}
          {/*</Button>*/}
          {/*</CardActions>*/}
          {/*</Card>*/}
          {/*} {...a11yProps(1)} />*/}
          {/*<Tab label="Item Three" icon={<LanguageIcon />} {...a11yProps(2)} />*/}
          {/*<Tab label="Item Four" icon={<LanguageIcon />} {...a11yProps(3)} />*/}
          {/*<Tab label="Item Five" icon={<LanguageIcon />} {...a11yProps(4)} />*/}
          {/*<Tab label="Item Six" icon={<LanguageIcon />} {...a11yProps(5)} />*/}
          {/*<Tab label="Item Seven" icon={<LanguageIcon />} {...a11yProps(6)} />*/}
          {/*</Tabs>*/}
          {/*</Card>*/}
        </ExpansionPanelDetails>
      </ExpansionPanel>
    )
  } else {
    return <Box>No results</Box>
  }
};

const mapDispatchToProps = dispatch => ({
  setIsHoverAction: shop => dispatch(setIsHoverAction(shop)),
  setIsOpenAction: (isOpen, shop) => dispatch(setIsOpenAction(isOpen, shop))
});

export default connect(null, mapDispatchToProps)(ListPinItem);
