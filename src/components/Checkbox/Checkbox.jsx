import React from 'react';
import {connect} from "react-redux";
import Checkbox from "@material-ui/core/Checkbox";
import {createStructuredSelector} from "reselect";
import {selectIsSelectAll, selectSelectedPins} from "../../redux/pins/pins.selectors";
import {deselectAllPinsAction, selectAllPinsAction, selectPinAction} from "../../redux/pins/pins.actions";
import './checkbox.scss';

const CheckboxContainer = ({
                             pin={shopId:-1},
                             action='selectPin',
                             selectedPins,
                             title,
                             isSelectAll,
                             selectPinAction,
                             selectAllPinsAction,
                             deselectAllPinsAction,
}) => {

  console.log('RENDER CHECKBOX');

  const checkIfPinIsSelected = () => {
    if (action === 'selectPin') {
      return !!(selectedPins.find((pinItem) => pinItem.shopId === pin.shopId));
    }
    if (action === 'selectAllPins') {
      return isSelectAll;
    }
  };

  const handleCheck = (pin) => () => {
    if (action === 'selectPin') {
      selectPinAction(pin);
    }
    if (action === 'selectAllPins') {
      if (isSelectAll) {
        deselectAllPinsAction();
      } else {
        selectAllPinsAction();
      }
    }
  };


  return (
    <Checkbox
      edge="end"
      color={'default'}
      onChange={handleCheck(pin)}
      checked={checkIfPinIsSelected()}
      title={title}
      // inputProps={{'aria-labelledby': labelId}}
      //onMouseEnter={handleMouseOver(pin)}
      //onMouseLeave={handleMouseOut(pin)}
      className={`checkbox ${checkIfPinIsSelected() ? 'checked' : ''}`}
    />
  )
};

const mapStateToProps = createStructuredSelector({
  selectedPins: selectSelectedPins,
  isSelectAll: selectIsSelectAll
});

const mapActionsToProps = dispatch => ({
  selectPinAction: pin => dispatch(selectPinAction(pin)),
  selectAllPinsAction: () => dispatch(selectAllPinsAction()),
  deselectAllPinsAction: () => dispatch(deselectAllPinsAction())
});

export default connect(mapStateToProps, mapActionsToProps)(CheckboxContainer);
