import React from 'react';
import Box from "@material-ui/core/Box";
import './sidebarRight.scss';
import Cart from "../Cart/Cart";

const SidebarRight = ({rightOpen}) => {

  return (
    <Box className={`${rightOpen ? 'sidebar-right open' : 'sidebar-right'}`}>
      <Cart/>
    </Box>
  )
};

export default SidebarRight;
