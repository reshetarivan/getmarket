import React from "react";
import './logo.scss';

const Logo = ({size='', clickable=true}) => {
  return (
    <div className={`logo ${size}`}>
      <a href={'/'} className={'link'} onClick={clickable ? ()=>{} : e => e.preventDefault()}>
        <span className={'logoColor'}>Get</span>Market
      </a>
      <a href={'/'} className={'link mobile'} onClick={clickable ? ()=>{} : e => e.preventDefault()}>
        <span className={'logoColor'}>G</span>M
      </a>
    </div>
  )
};

export default Logo;
