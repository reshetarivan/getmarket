import React from 'react';
import Checkbox from "../Checkbox/Checkbox";
import './counters.scss';
import {connect} from "react-redux";
import {createStructuredSelector} from "reselect";
import {selectSelectedPins} from "../../redux/pins/pins.selectors";

const Counters = ({ loading, shops, selectedPins }) => {

  return (
    <div className={'counters'}>
      <div>
        <Checkbox
          edge="end"
          color={'default'}
          action={'selectAllPins'}
          //onChange={onChange}
          //selectedPins={selectedPins}
          //isSelectAll={isSelectAll}
          //checked={didSelectAllPins}
          title={'Select all'}
        />
      </div>
      <div>Selected:
        <span className={"counter"}>
          {loading ? 0 : selectedPins.length}
        </span>
      </div>
      <div>Results:
        <span className={"counter"}>
          {loading ? 0 : shops.pins.totalItems}
        </span>
      </div>
      <div>Sellers:
        <span className={"counter"}>
          {loading ? 0 : shops.pins.totalShops}
        </span>
      </div>
    </div>
  )

};

const mapStateToProps = createStructuredSelector({
  selectedPins: selectSelectedPins,
});

export default connect(mapStateToProps)(Counters);
