import React from 'react';
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Radio from "@material-ui/core/Radio";
// import {useStateValue} from "../../state";
import './typeCheckboxes.scss';

export default () => {

  // let [mainState, dispatch] = useStateValue();

  const handleChangeType = (event) => {
    // dispatch({
    //   type: 'type',
    //   newFilter: event.target.value
    // });
  };

  return (
    <RadioGroup
      aria-label="type"
      name="type"
      // value={mainState.filters.type}
      onChange={handleChangeType}
      className={'containerType'}
    >
      <FormControlLabel
        value="p"
        control={<Radio className={'radioClass'} />}
        label="Product"
        labelPlacement="end"
        // checked={mainState.filters.type === 'p'}
      />
      <FormControlLabel
        value="s"
        control={<Radio className={'radioClass'}/>}
        label="Service"
        labelPlacement="end"
        // checked={mainState.filters.type === 's'}
      />
    </RadioGroup>
  )
}
