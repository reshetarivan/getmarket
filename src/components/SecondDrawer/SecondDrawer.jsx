import React from 'react';
import SwipeableDrawer from "@material-ui/core/SwipeableDrawer";
import {connect} from "react-redux";
import {setIsOpenAction} from "../../redux/reviews/reviews.actions";
import {createStructuredSelector} from "reselect";
import {selectIsOpen} from "../../redux/reviews/reviews.selectors";
import './secondDrawer.scss';

const SecondDrawer = ({isOpen, setIsOpenAction, children}) => {

  const toggleDrawer = (open) => event => {
    if (event && event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
      return;
    }
    setIsOpenAction(open);
  };

  return (
    <SwipeableDrawer
      open={isOpen}
      onClose={toggleDrawer(false)}
      onOpen={toggleDrawer(true)}
    >
      <div
        role="presentation"
        //onClick={toggleDrawer(false)}
        // onKeyDown={toggleDrawer(false)}
        className={'second-drawer'}
      >
        {children}
      </div>
    </SwipeableDrawer>
  )
};

const mapStateToProps = createStructuredSelector({
  isOpen: selectIsOpen
});

const mapDispatchToProps = dispatch => ({
  setIsOpenAction: isOpen => dispatch(setIsOpenAction(isOpen))
});

export default connect(mapStateToProps, mapDispatchToProps)(SecondDrawer);

