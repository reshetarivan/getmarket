import React, {useState} from 'react';
import {AppBar, makeStyles, Toolbar, Zoom} from "@material-ui/core";
import {Logo} from "../";
import './header.scss';
import InputSearch from "../InputSearch/InputSearch";
import IconButton from "@material-ui/core/IconButton";
import {AccountCircle} from "@material-ui/icons";
import MailIcon from '@material-ui/icons/Mail';
import MoreIcon from '@material-ui/icons/MoreVert';
import FavoriteIcon from '@material-ui/icons/Favorite';
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import Badge from "@material-ui/core/Badge";
import ButtonStyled from "../ButtonStyled/ButtonStyled";
import ButtonAccount from "../ButtonAccount/ButtonAccount";
import MonetizationOnIcon from '@material-ui/icons/MonetizationOn';
import Tooltip from "@material-ui/core/Tooltip";

const useStyles = makeStyles(theme => ({
  grow: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  logo: {
    display: 'none',
    [theme.breakpoints.up('sm')]: {
      display: 'block',
    },
  },
  search: {
    position: 'relative',
    marginRight: theme.spacing(2),
    marginLeft:theme.spacing(2),
    width: '100% !important',
    [theme.breakpoints.up('sm')]: {
      //marginLeft: theme.spacing(3),
      width: 'auto',
    },
  },
  sectionDesktop: {
    display: 'none',
    [theme.breakpoints.up('md')]: {
      display: 'grid',
      gridGap: '16px',
      gridTemplateColumns: '1fr 1fr 1fr 1fr',
    },
  },
  sectionMobile: {
    display: 'flex',
    [theme.breakpoints.up('md')]: {
      display: 'none',
    },
  },
}));

export default ({params}) => {

  console.log('RENDER HEADER');

  const classes = useStyles();
  const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = React.useState(null);
  const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);


  const handleMobileMenuClose = () => {
    setMobileMoreAnchorEl(null);
  };

  const handleMenuClose = () => {
    //setAnchorEl(null);
    handleMobileMenuClose();
  };

  const handleMobileMenuOpen = event => {
    setMobileMoreAnchorEl(event.currentTarget);
  };

  const mobileMenuId = 'primary-account-menu-mobile';
  const renderMobileMenu = (
    <Menu
      anchorEl={mobileMoreAnchorEl}
      anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
      id={mobileMenuId}
      keepMounted
      transformOrigin={{ vertical: 'top', horizontal: 'right' }}
      open={isMobileMenuOpen}
      onClose={handleMobileMenuClose}
    >
      <MenuItem>
        <IconButton aria-label="show 11 new notifications" color="inherit">
          <Badge badgeContent={11} color="secondary">
            <FavoriteIcon />
          </Badge>
        </IconButton>
        <p>Notifications</p>
      </MenuItem>
      <MenuItem>
        <IconButton aria-label="show 4 new mails" color="inherit">
          <Badge badgeContent={4} color="secondary">
            <MailIcon />
          </Badge>
        </IconButton>
        <p>Messages</p>
      </MenuItem>
      <MenuItem onClick={'handleProfileMenuOpen'}>
        <IconButton
          aria-label="account of current user"
          aria-controls="primary-search-account-menu"
          aria-haspopup="true"
          color="inherit"
        >
          <AccountCircle />
        </IconButton>
        <p>Profile</p>
      </MenuItem>
    </Menu>
  );


  return (
    <div>
      <AppBar position="fixed" elevation={'none'} className={'app-bar'} color={"default"}>
        <Toolbar className={'toolbar'}>
          <div className={'logo-box'}>
            <Logo size={'small'} className={classes.title}/>
          </div>
          <div className={classes.search}>
            <InputSearch isAutoFocus={false} />
          </div>
          <div className={classes.grow} />
          <div className={classes.sectionDesktop}>
            <ButtonStyled type={'text'} variant={'text'} color={'secondary'} size={'large'} startIcon={<MonetizationOnIcon/>}>
              Sell
            </ButtonStyled>
            <ButtonStyled color="inherit" type="icon">
              <Tooltip title="Favorite" TransitionComponent={Zoom} placement="bottom" arrow>
                <Badge badgeContent={17} color="secondary">
                  <FavoriteIcon />
                </Badge>
              </Tooltip>
            </ButtonStyled>
            <ButtonStyled color="inherit" type="icon">
              <Tooltip title="Messages" TransitionComponent={Zoom} placement="bottom" arrow>
                <Badge badgeContent={4} color="secondary">
                  <MailIcon />
                </Badge>
              </Tooltip>
            </ButtonStyled>
            <ButtonAccount />
          </div>
          <div className={classes.sectionMobile}>
            <ButtonStyled
              ariaLabel="show more"
              ariaControls={mobileMenuId}
              ariaHaspopup="true"
              onClick={handleMobileMenuOpen}
              color="inherit"
              type="icon"
            >
              <MoreIcon />
            </ButtonStyled>
          </div>
            {/*<Grid item md={3} xs={12}>*/}
              {/*<FilterSelect filterHeight={'small'} source={citiesUrl} filterOptions={'cities'} fieldId={'city'} fieldName={'City'} fieldValue={mainState.filters.city} handleChangeFilter={handleChangeFilter}/>*/}
            {/*</Grid>*/}
        </Toolbar>
      </AppBar>
      {renderMobileMenu}
    </div>
  )
}
