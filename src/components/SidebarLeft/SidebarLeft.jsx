import React from 'react';
import Box from "@material-ui/core/Box";
import './sidebarLeft.scss';
import List from "@material-ui/core/List";
import {Zoom} from "@material-ui/core";
import FilterLocation from "../Filters/FilterLocation/FilterLocation";
import FilterCategory from "../Filters/FilterCategory/FilterCategory";
import FilterPrice from "../Filters/FilterPrice/FilterPrice";
import FilterCondition from "../Filters/FilterCondition/FilterCondition";
import FilterDelivery from "../Filters/FilterDelivery/FilterDelivery";
import FilterSize from "../Filters/FilterSize/FilterSize";
import FilterColor from "../Filters/FilterColor/FilterColor";
import Tooltip from "@material-ui/core/Tooltip";
import TuneIcon from '@material-ui/icons/Tune';
import Backdrop from "@material-ui/core/Backdrop";
import ShoppingCartIcon from "../Cart/Cart";


const SidebarLeft = ({open, toggleDrawer}) => {

  const handleFiltersOpen = () => {
    toggleDrawer();
  };

  return (
    <Box className={`${open ? 'sidebar-left open' : 'sidebar-left'}`}>
      <Box className={'cart-title'}>
      </Box>
      <List className={`${open ? 'filters open' : 'filters'}`}>
        <FilterLocation/>
        <FilterCategory/>
        <FilterPrice/>
        <FilterCondition/>
        <FilterDelivery/>
        <FilterSize/>
        <FilterColor/>
      </List>
      <Tooltip title="Open Filters" TransitionComponent={Zoom} placement="right" arrow>
        <Box className={`filters-mobile ${open ? 'mobile-open': ''}`} onClick={handleFiltersOpen}>
          <TuneIcon className={'filters-icon mobile'}/>
          <Box className={'filters-mobile-title'}>Filters</Box>
          <Box className={'filters-mobile-amount'}>2</Box>
          <Box className={'filters-mobile-active'}>active</Box>
        </Box>
      </Tooltip>
      <Backdrop className={'backdrop'} open={open} onClick={handleFiltersOpen}> </Backdrop>
    </Box>
  )
};

export default SidebarLeft;
