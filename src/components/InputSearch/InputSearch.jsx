import React from 'react';
import _ from 'lodash';
import deburr from 'lodash/deburr';
import Autosuggest from 'react-autosuggest';
import match from 'autosuggest-highlight/match';
import parse from 'autosuggest-highlight/parse';
import {
  Paper,
  MenuItem,
  IconButton,
  InputAdornment,
  FormControl
} from '@material-ui/core/';
import {Clear, Search} from "@material-ui/icons";
// import axios from "axios";
import FormHelperText from "@material-ui/core/FormHelperText";
import TextField from "@material-ui/core/TextField";
import {connect} from "react-redux";
import {createStructuredSelector} from "reselect";
import {
  selectFiltersCategory,
  selectFiltersIsError,
  selectFiltersIsLoading,
  selectFiltersSearch,
  selectFiltersSearchImg,
  selectFiltersSuggestions,
  selectFiltersType
} from "../../redux/filters/filters.selectors";
import {
  clearSearchAction,
  inputSearchAction,
  selectSuggestionAction,
  startGetSuggestionAction
} from "../../redux/filters/filters.actions";
// import {debounce} from 'lodash';
// import Spinner from "../Spinner/Spinner";
import CircularProgress from "@material-ui/core/CircularProgress";
import './inputSearch.scss';



const InputSearch = ({
                       filtersSearch,
                       filtersSearchImg,
                       filtersType,
                       filtersCategory,
                       filtersSuggestions=[],
                       filtersIsLoading,
                       filtersIsError,
                       selectSuggestionAction,
                       inputSearchAction,
                       clearSearchAction
                     }) => {

  console.log('filtersIsLoading', filtersIsLoading);
  console.log('filtersSearch', filtersSearch);

  const handleSuggestionsFetchRequested = ({ value }) => {
    const inputValue = deburr(value.trim()).toLowerCase();
    const inputLength = inputValue.length;
    let count = 0;

    return inputLength === 0 || filtersSuggestions === []
      ? []
      : (
        filtersSuggestions.filter(suggestion => {
          //const keep = count < 10 && suggestion.name.slice(0, inputLength).toLowerCase() === inputValue.toLowerCase();
          const keep = count < 10 && suggestion.name.toLowerCase().indexOf(inputValue.toLowerCase() >= 0);

          if (keep) {count += 1;}
          return keep;
        })
      )
  };

  const handleSuggestionsClearRequested = () => {
    startGetSuggestionAction();
  };

  const handleSelectSuggestion = (suggestion) => () => {
    const {name, type, category, img} = suggestion;
    selectSuggestionAction({search: name, searchImg: img, type, category});
  };

  const renderSuggestion = (suggestion, { query, isHighlighted }) => {
    const matches = match(suggestion.name, query);
    const parts = parse(_.startCase(suggestion.name), matches);

    return (
      <MenuItem selected={isHighlighted}
                component="div"
                onClick={handleSelectSuggestion(suggestion)}
      >
        {/*<img className={'searchIcon'} alt={'Icon'} src={`/img/items/${suggestion.img}`} />*/}
        <div>
          {parts.map(part => (
            <span key={part.text} style={{ fontWeight: part.highlight ? 600 : 400 }}>
              {part.text}
            </span>
          ))}
        </div>
        <div className={'descr'}>
          [{_.startCase(suggestion.category)}]
        </div>
      </MenuItem>
    );
  };


  const getSuggestionValue = (suggestion) => {
    return _.startCase(suggestion.name);
  };

  const handleChange = () => (event) => {
    inputSearchAction(_.startCase(event.target.value));
  };

  const handleClickClearIcon = () => {
    clearSearchAction();
  };

  const handleSearch = () => {
    return true;
  };

  const renderError = () => {
    if (filtersIsError) {
      return <FormHelperText className="component-error-text">{filtersIsError}</FormHelperText>
    }
  };

  const renderStartAdornment = () => {
    let searchImage = 'noimage.png';
    if (filtersSearchImg) {
      searchImage = filtersSearchImg;
    }
    return (
      <InputAdornment position="start" className={'startInputAdornment'}>
        <img className={'searchIcon'} alt={'Icon'} src={`/img/items/${searchImage}`} />
      </InputAdornment>
    )
  };


  const renderInputComponent = (inputProps) => {
    const {inputRef = () => {}, ref, ...other } = inputProps;

    return (
          <FormControl variant="outlined" className={'search'}>
            <TextField
              fullWidth
              required
              InputProps={{
                //value: filtersSearch,
                onChange: handleChange(),
                inputRef: node => {
                  ref(node);
                  inputRef(node);
                },
                classes: {
                  input: 'input'
                },
                // startAdornment: (
                //   renderStartAdornment()
                // ),
                endAdornment: (
                  <InputAdornment position="end">
                    <IconButton
                      edge="end"
                      aria-label="Clear search input"
                      onClick={handleClickClearIcon}
                    >
                      {filtersSearch ? <Clear/> : <Search/> }
                    </IconButton>
                    {filtersIsLoading ? <CircularProgress className={'spinner'}/> : (filtersSearch ? (
                      <IconButton
                        edge="end"
                        aria-label="Search"
                        onClick={handleSearch}
                      >
                        <Search/>
                      </IconButton>
                      ) : '')
                    }
                  </InputAdornment>
                )
              }}
              {...other}
            />
          </FormControl>
    );
  };

  const autosuggestProps = {
    renderInputComponent,
    suggestions: filtersSuggestions,
    onSuggestionsFetchRequested: handleSuggestionsFetchRequested,
    onSuggestionsClearRequested: handleSuggestionsClearRequested,
    getSuggestionValue,
    renderSuggestion,
  };

  const handleClose = () => (event) => {
    console.log('newValue', event.target.value);
  };


  return (
    <div className={'inputSearch'}>
      <div className={`inputSearchContainer ${filtersSearch ? 'active-field' : ''}`}>
        <Autosuggest
          {...autosuggestProps}
          inputProps={{
            name: 'search',
            value: `${_.startCase(filtersSearch)}`,
            id: 'react-autosuggest-simple',
            // label: 'Search for ...',
            //placeholder: 'Please enter product or service',
            onChange: ()=>{},
            variant: "outlined",
            margin: "normal",
            fullWidth: true,
            autoFocus: !filtersSearch,
            // required: true,
          }}
          theme={{
            container: 'searchContainer',
            suggestionsContainerOpen: 'suggestionsContainerOpen',
            suggestionsList: 'suggestionsList',
            suggestion: 'suggestion',
          }}
          renderInputComponent={renderInputComponent}
          renderSuggestionsContainer={options => (
            <Paper {...options.containerProps} >
              {options.children}
            </Paper>
          )}
        />
      </div>
      {renderError()}
    </div>
  );
};

const mapStateToProps = createStructuredSelector({
  filtersSearch: selectFiltersSearch,
  filtersSearchImg: selectFiltersSearchImg,
  filtersType: selectFiltersType,
  filtersCategory: selectFiltersCategory,
  filtersSuggestions: selectFiltersSuggestions,
  filtersIsLoading: selectFiltersIsLoading,
  filtersIsError: selectFiltersIsError,
});

const mapActionsToProps = dispatch => ({
  selectSuggestionAction: request => dispatch(selectSuggestionAction(request)),
  clearSearchAction: () => dispatch(clearSearchAction()),
  inputSearchAction: (input) => dispatch(inputSearchAction(input))
});

export default connect(mapStateToProps, mapActionsToProps)(InputSearch);
