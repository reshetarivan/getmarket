import React from 'react';
import './homePage.scss';
import {Box, Button, Container, Grid} from "@material-ui/core";
import {InputSearch, Logo} from "../../components";
import {connect} from "react-redux";
import {createStructuredSelector} from "reselect";
import SearchIcon from '@material-ui/icons/Search';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import {selectFiltersCategory, selectFiltersSearch, selectFiltersType} from "../../redux/filters/filters.selectors";
import Footer from "../../components/Footer/Footer";

const HomePage = ({history, filtersSearch, filtersType, filersCategory}) => {

  const handleSearch = () => {
    history.push('/s');
  };

  return (
    <Box className={'home-page'}>
      <Box className={'login-box'}>
        <Button
          size="medium"
          color="default"
          variant="outlined"
          className={'sign-in-button'}
        >
          <ExitToAppIcon className={'sign-in-icon'}/>
          Sign In
        </Button>
      </Box>
      <Container maxWidth="sm" className={'search-container'}>
        <Logo clickable={false}/>
        <div className={'slogan'}>Buy & Sell Anything By Auctions</div>
        <div className={'search-form'}>
          <Grid container spacing={1}>
            <Grid item xs={12}>
              <InputSearch/>
            </Grid>
            <Grid item xs={12} className={'search-button-box'}>
              <Button
                variant="contained"
                className={`submit-search ${filtersSearch ? 'active' : ''}`}
                type={'button'}
                disabled={!filtersSearch}
                onClick={handleSearch}
              >
                <SearchIcon className={'search-icon'}/>
                Search
              </Button>
              <input type={'hidden'} name={'type'} value={filtersType}/>
              <input type={'hidden'} name={'cat'} value={filersCategory}/>
            </Grid>
          </Grid>
        </div>
      </Container>
      <Footer/>
    </Box>
  );

};

const mapStateToProps = createStructuredSelector({
  filtersSearch: selectFiltersSearch,
  filtersType: selectFiltersType,
  filersCategory: selectFiltersCategory
});

export default connect(mapStateToProps)(HomePage);

