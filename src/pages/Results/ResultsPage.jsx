import React, {useCallback, useEffect, useState} from 'react';
import axios from "axios";
import {connect} from "react-redux";
import PropTypes from 'prop-types';
import {createStructuredSelector} from "reselect";
import {DrawerHandle, InfoPanel, Marker} from "../../components";
import CircularProgress from "@material-ui/core/CircularProgress";
import {selectFiltersAll} from "../../redux/filters/filters.selectors";
import {selectPinsAll, selectPinsIsLoading} from "../../redux/pins/pins.selectors";
import {selectGeoLatLng} from "../../redux/geo/geo.selectors";
import {startGetPinsAction} from "../../redux/pins/pins.actions";
import {
  changeBoundsAction,
  failGetGeoAction,
  startGetGeoAction,
  successGetGeoAction
} from "../../redux/geo/geo.actions";
import './resultsPage.scss';
import Header from "../../components/Header/Header";
import SidebarLeft from "../../components/SidebarLeft/SidebarLeft";
import Box from "@material-ui/core/Box";
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import FilterListIcon from '@material-ui/icons/FilterList';
import Footer from "../../components/Footer/Footer";
import Button from "@material-ui/core/Button";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Typography from "@material-ui/core/Typography";
import {IconButton, useMediaQuery, Zoom} from "@material-ui/core";
import {MainConfig} from "../../config/main.config";
import TuneIcon from '@material-ui/icons/Tune';
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import SidebarRight from "../../components/SidebarRight/SidebarRight";
import Tooltip from "@material-ui/core/Tooltip";

const ResultsPage = ({
                        startGetPinsAction,
                        startGetGeoAction,
                        successGetGeoAction,
                        failGetGeoAction,
                        changeBoundsAction,
                        filters,
                        shops,
                        shopsIsLoading,
                        geoLatLng
}) => {

  console.log('RENDER RESULTS', shops);
  const [open, setOpen] = useState(true);
  const toggleDrawer = () => {
    setOpen(!open);
  };

  const [tabValue, setTabValue] = React.useState(1);
  const handleTabChange  = (event, newValue) => {
    if (newValue === 0) {
      toggleDrawer();
      newValue = tabValue;
    }
    setTabValue(newValue);
  };
  const screenSize = useMediaQuery(MainConfig.LG);
  useEffect(()=> {
    setOpen(screenSize);
  }, [screenSize]);

  useEffect(()=> {
    startGetPinsAction(filters);
  }, []);

  const getGeoLocation = (position) => {
    console.log('RENDER getGeoLocation', position);
    const payload = {
      lat: position.coords.latitude,
      lng: position.coords.longitude
    };
    successGetGeoAction(payload);
  };

  const getApiGeoLocation = useCallback(() => {
    axios.post(`https://www.googleapis.com/geolocation/v1/geolocate?key=${process.env.REACT_APP_GOOGLE_MAPS_KEY}`)
      .then((response) => {
        const payload = {
          lat: response.data.location.lat,
          lng: response.data.location.lng
        };
        successGetGeoAction(payload);
      })
      .catch(err => {
        failGetGeoAction(err);
      });
  }, []);



  useEffect(() => {
    startGetGeoAction();
    navigator.geolocation.getCurrentPosition(getGeoLocation, getApiGeoLocation);
  }, []);


  function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
      <Typography
        component="div"
        role="tabpanel"
        hidden={value !== index}
        id={`wrapped-tabpanel-${index}`}
        aria-labelledby={`wrapped-tab-${index}`}
        {...other}
      >
        {value === index && <Box p={3}>{children}</Box>}
      </Typography>
    );
  }

  TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
  };


  function a11yProps(index) {
    return {
      id: `simple-tab-${index}`,
      'aria-controls': `simple-tabpanel-${index}`,
    };
  }

  return (
    <Box className={'results-page'}>
      <Header/>
      <Box className={'body-box'} >
        <SidebarLeft open={open} toggleDrawer={toggleDrawer}/>
        <Box className={'content-box'}>
          <Box className={'content-header'}>
            {/*<Button onClick={toggleDrawer} edge="start">*/}
              {/*{open ? <ChevronLeftIcon /> : <FilterListIcon />}*/}
            {/*</Button>*/}
            <AppBar position="static" className={'appbar'}>
              <Tabs value={tabValue} onChange={handleTabChange} className={'tabs'}>
                  <Tab
                    label={open ? (
                      <Tooltip title="Hide Filters" TransitionComponent={Zoom} placement="top" arrow>
                        <TuneIcon />
                      </Tooltip>
                    ) : (
                      <Tooltip title="Show Filters" TransitionComponent={Zoom} placement="top" arrow>
                        <TuneIcon />
                      </Tooltip>
                      )}
                    {...a11yProps(0)}
                  />
                <Tab label="All (236)" {...a11yProps(1)} />
                <Tab label="Buy (43)" {...a11yProps(2)} />
                <Tab label="Rent (22)" {...a11yProps(3)} />
              </Tabs>
            </AppBar>
            <Box>
              {/*Sort*/}
            </Box>
          </Box>
          <Box className={'content-body'}>
            <TabPanel value={tabValue} index={1}>
              This section is under development. Please, come later.
            </TabPanel>
            <TabPanel value={tabValue} index={2}>
              This section is under development. Please, come later.
            </TabPanel>
            <TabPanel value={tabValue} index={3}>
              This section is under development. Please, come later.
            </TabPanel>
          </Box>
        </Box>
        <SidebarRight/>
      </Box>
      <Footer/>
    </Box>
  );
};


const mapStateToProps = createStructuredSelector({
  filters: selectFiltersAll,
  shops: selectPinsAll,
  shopsIsLoading: selectPinsIsLoading,
  geoLatLng: selectGeoLatLng
});


const mapActionsToProps = dispatch => ({
  startGetPinsAction: query => dispatch(startGetPinsAction(query)),
  startGetGeoAction: query => dispatch(startGetGeoAction(query)),
  successGetGeoAction: query => dispatch(successGetGeoAction(query)),
  failGetGeoAction: query => dispatch(failGetGeoAction(query)),
  changeBoundsAction: query => dispatch(changeBoundsAction(query)),
});

export default connect(mapStateToProps, mapActionsToProps)(ResultsPage);
