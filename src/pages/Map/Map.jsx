import React, {useCallback, useEffect, useState} from 'react';
// import queryString from 'query-string';
import GoogleMapReact from 'google-map-react';
import axios from "axios";
import {connect} from "react-redux";
import {createStructuredSelector} from "reselect";
import {DrawerHandle, InfoPanel, Marker} from "../../components";
import CircularProgress from "@material-ui/core/CircularProgress";
import {selectFiltersAll} from "../../redux/filters/filters.selectors";
import {selectPinsAll, selectPinsIsLoading} from "../../redux/pins/pins.selectors";
import {selectGeoLatLng} from "../../redux/geo/geo.selectors";
import {startGetPinsAction} from "../../redux/pins/pins.actions";
import {
  changeBoundsAction,
  failGetGeoAction,
  startGetGeoAction,
  successGetGeoAction
} from "../../redux/geo/geo.actions";
import './map.scss';


const MapContainer = ({
                        startGetPinsAction,
                        startGetGeoAction,
                        successGetGeoAction,
                        failGetGeoAction,
                        changeBoundsAction,
                        filters,
                        shops,
                        shopsIsLoading,
                        geoLatLng
}) => {

  console.log('RENDER MAP', shops);

  useEffect(()=> {
    startGetPinsAction(filters);
  }, []);

  //let params = queryString.parse(props.location.search);

  const getGeoLocation = (position) => {
    console.log('RENDER getGeoLocation', position);
    const payload = {
      lat: position.coords.latitude,
      lng: position.coords.longitude
    };
    successGetGeoAction(payload);
  };

  const getApiGeoLocation = useCallback(() => {
    console.log('RENDER getApiGeoLocation');

    axios.post(`https://www.googleapis.com/geolocation/v1/geolocate?key=${process.env.REACT_APP_GOOGLE_MAPS_KEY}`)
      .then((response) => {
        const payload = {
          lat: response.data.location.lat,
          lng: response.data.location.lng
        };
        successGetGeoAction(payload);
      })
      .catch(err => {
        console.error(err.message);
        failGetGeoAction(err);
      });
      //axios.get(`https://ipapi.co/json/`).then((response) => {});
  }, []);


  useEffect(() => {
    startGetGeoAction();
    navigator.geolocation.getCurrentPosition(getGeoLocation, getApiGeoLocation);
  }, []);


  const renderMarkers = (shops) => {

    console.log({shops});
    let res = [];
    if (shops.pins?.data?.length > 0) {
      res = shops.pins?.data?.map(shop => {
        return (
          <Marker
            shop={shop}
            lat={shop.shopLat}
            lng={shop.shopLng}
            isUserMarker={false}
            key={shop.shopId}
          />
        );
      })
    }
    return res;
  };


  const _onBoundsChange = (event) => {
    console.log('onBoundsChange', event);
    const bounds = event.bounds;
    const payload = {
      ne_lat: bounds.ne.lat,
      ne_lng: bounds.ne.lng,
      sw_lat: bounds.sw.lat,
      sw_lng: bounds.sw.lng,
    };
    changeBoundsAction(payload);
  };


  const _onMapInit = (event) => {
    const map = event.map;
    const bounds = map.getBounds();
    const southWest = bounds.getSouthWest();
    const northEast = bounds.getNorthEast();
    const payload = {
      ne_lat: northEast.lat(),
      ne_lng: northEast.lng(),
      sw_lat: southWest.lat(),
      sw_lng: southWest.lng(),
    };
    changeBoundsAction(payload);
  };


  const handleMapClick = (e) => {
    console.log('handleMapClick', e);
  };


  const renderGoogleMap = () => {
    console.log('RENDER renderGoogleMap', shops);
    if (geoLatLng.lng && geoLatLng.lat) {
      return <GoogleMapReact
        bootstrapURLKeys={{key: process.env.REACT_APP_GOOGLE_MAPS_KEY}}
        center={{
          lat: geoLatLng.lat,
          lng: geoLatLng.lng,
        }}
        defaultZoom={11}
        onGoogleApiLoaded={_onMapInit}
        onChange={_onBoundsChange}
        onClick={handleMapClick}
        yesIWantToUseGoogleMapApiInternals
    //     //options={createMapOptions}
    //     // onChildClick={this._onChildClick}
    //     // onChildMouseEnter={this._onChildMouseEnter}
    //     // onChildMouseLeave={this._onChildMouseLeave}
    //     // margin={[K_MARGIN_TOP, K_MARGIN_RIGHT, K_MARGIN_BOTTOM, K_MARGIN_LEFT]}
    //     // hoverDistance={K_HOVER_DISTANCE}
    //     // distanceToMouse={this._distanceToMouse}
      >
        <Marker
          lat={parseFloat(geoLatLng.lat)}
          lng={parseFloat(geoLatLng.lng)}
          shop={{
            shopLat: parseFloat(geoLatLng.lat),
            shopLng: parseFloat(geoLatLng.lng),
            shopName: 'Your location',
            shopImg:'icons/user.svg',
            shopId: 0
          }}
          isUserMarker={true}
          key={0}
        />
        {renderMarkers(shops)}
      </GoogleMapReact>
    } else {
      return <CircularProgress className={'circularProgress'}/>
    }
  };


  const [open, setOpen] = useState(true);
  return (
    <div className={'googleMapStyle'}>
      <InfoPanel
        shops={shops}
        open={open}
        loading={shopsIsLoading}
      />
      <DrawerHandle open={open} setOpen={setOpen}/>
      {/*{renderGoogleMap()}*/}
    </div>
  );
};


const mapStateToProps = createStructuredSelector({
  filters: selectFiltersAll,
  shops: selectPinsAll,
  shopsIsLoading: selectPinsIsLoading,
  geoLatLng: selectGeoLatLng
});


const mapActionsToProps = dispatch => ({
  startGetPinsAction: query => dispatch(startGetPinsAction(query)),
  startGetGeoAction: query => dispatch(startGetGeoAction(query)),
  successGetGeoAction: query => dispatch(successGetGeoAction(query)),
  failGetGeoAction: query => dispatch(failGetGeoAction(query)),
  changeBoundsAction: query => dispatch(changeBoundsAction(query)),
});

export default connect(mapStateToProps, mapActionsToProps)(MapContainer);
