const htmlWebpackPlugin = require('html-webpack-plugin');

const htmlWebpackConfig = new htmlWebpackPlugin({
  template: __dirname + '/src/index.html',
  filename: 'index.html',
  inject: 'body'
});

module.exports = {
  module: {
    rules: [
      {
        test: /\.scss$/,
        use: ['style-loader', 'css-loader', 'sass-loader']
      }
    ]
  },
  plugins: [ htmlWebpackConfig ],
  devServer: {
    inline: true,
    historyApiFallback: {
      index: '/'
    }
  }
};
